'use strict';

module.exports = {
    sqldb: {
        username: 'root',
        password: '123456',
        database: 'liquidityex_schema',
        host: 'localhost',
        port: '3306',
        dialect: 'mysql',
    },
    mongodb: 'mongodb://localhost/liquidityex',
    twoFactorsAuth: {
        authyApiKey: 'GD3qP1X5KOIYcxHkoRU3Tgh5oFr64t8K',
    },
    mq: {
        host: 'amqp://localhost',
        schedulerExchange: 'lqx.scheduler.local',
    },
    origin: 'http://dev.liquidityex.com:3000',
    oauth: {
        bondora: {
            clientId: '0747fd5618ad48deadc9cb920ff5d5ee',
            clientSecret: 'R4a6coRiyfy2Jc9LTOASZDu9LoQSlNTNLgLFkzvgYA046mTi',
            callbackUrl: 'http://dev.liquidityex.com:3000/bondora/oauth-callback',
        },
    },
};
