'use strict';

module.exports = {
    port: 3000,
    session: {
        secret: 'liquidity-exchange',
    },
    twoFactorsAuth: {
        enabled: false,
    },
};
