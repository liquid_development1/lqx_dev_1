'use strict';

module.exports = {
    sqldb: {
        username: 'root',
        password: '123456',
        database: 'liquidityex_schema',
        host: 'localhost',
        port: '3306',
        dialect: 'mysql',
    },
    mongodb: 'mongodb://localhost/liquidityex',
    twoFactorsAuth: {
        authyApiKey: 'GD3qP1X5KOIYcxHkoRU3Tgh5oFr64t8K',
    },
    mq: {
        host: 'amqp://localhost',
        schedulerExchange: 'lqx.scheduler.local',
    },
    origin: 'http://localhost:3000',
    oauth: {
        bondora: {
            clientId: 'ac634a7335be4d07b4f586d7232d6375',
            clientSecret: 'EybUwF8HqCdPTElytVUoSUb8bo3Sx2IeTrYxZ4HIbqG9ef9W',
            callbackUrl: 'http://localhost:3000/bondora/oauth-callback',
        },
    },
};
