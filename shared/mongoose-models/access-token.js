'use strict';

const mongoose = require('mongoose');

const accessTokenSchema = new mongoose.Schema({
    createdAt: {
        type: Date,
        expires: '15d',
        required: true,
    },
    type: {
        type: String,
        unique: true,
        required: true,
    },
    value: {
        type: String,
        required: true,
    },
});

mongoose.model('AccessToken', accessTokenSchema);
