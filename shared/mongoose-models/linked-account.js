'use strict';

const mongoose = require('mongoose');

const linkedAccountSchema = new mongoose.Schema({
    type: {
        unique: true,
        type: String,
        required: true,
    },
    refreshToken: {
        type: String,
        required: true,
    },
});

mongoose.model('LinkedAccount', linkedAccountSchema);
