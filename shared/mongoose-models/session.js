'use strict';

const mongoose = require('mongoose');

const sessionSchema = new mongoose.Schema({
    touchedAt: {
        type: Date,
        expires: '30m',
        required: true,
    },
    sid: {
        type: String,
        unique: true,
    },
    userId: {
        type: Number,
        required: true,
    },
    user: {
        type: Object,
        required: true,
    },
    hash: {
        type: String,
        required: true,
    },
    verified: {
        type: Boolean,
        required: true,
    },
});

sessionSchema.methods.toJSON = function() {
    var obj = this.toObject();
    delete obj.touchedAt;
    delete obj._id;
    delete obj.__v;

    return obj;
};

mongoose.model('Session', sessionSchema);
