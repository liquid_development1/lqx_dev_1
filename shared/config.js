'use strict';

const _ = require('lodash');
const argv = require('yargs').argv;
const path = require('path');
const defaults = require('./env/_default');

const profile = argv.p || argv.profile || process.env.NODE_ENV || 'local';
const profileConf = require(path.resolve(__dirname, './env', profile));

module.exports = _.defaultsDeep(profileConf, defaults);
