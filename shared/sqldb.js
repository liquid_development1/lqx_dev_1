'use strict';

const glob = require('glob');
const path = require('path');
const Sequelize = require('sequelize');

const sqldb = require('./config').sqldb;

const sequelize = new Sequelize(sqldb.database, sqldb.username, sqldb.password, sqldb);
const db = {
    sequelize: sequelize,
    Sequelize: Sequelize,
};

glob.sync(path.resolve(__dirname, './sequelize-models/*.js')).forEach(function(def) {
    var model = sequelize['import'](def);
    db[model.name] = model;
});

module.exports = db;
