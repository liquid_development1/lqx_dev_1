'use strict';

const glob = require('glob');
const mongoose = require('mongoose');
const path = require('path');

const mongodb = require('./config').mongodb;

mongoose.Promise = global.Promise;
mongoose.set('debug', true);
mongoose.connect(mongodb);

glob.sync(path.resolve(__dirname, './mongoose-models/*.js')).forEach(require);

module.exports = mongoose;
