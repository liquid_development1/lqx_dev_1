'use strict';

const mongoose = require('mongoose');
const request = require('request');

const config = require('../config');

const AccessToken = mongoose.model('AccessToken');
const LinkedAccount = mongoose.model('LinkedAccount');

const EXPIRY = 1000 * 60 * 60 * 24 * 3; // 3 days

exports.getAccessToken = function() {
    return AccessToken
        .where({
            type: 'bondora',
        })
        .findOne()
        .exec()
        .then((accessToken) => {
            if (accessToken) {
                // check expire
                if (!isExpired(accessToken.createdAt)) {
                    return accessToken.value;
                }
            }

            return getAccessTokenByRefreshToken();
        });
};

function isExpired(createdAt) {
    return createdAt < Date.now() - EXPIRY;
}

function getAccessTokenByRefreshToken() {
    return LinkedAccount
        .where({
            type: 'bondora',
        })
        .findOne()
        .exec()
        .then((bondoraLinkedAccount) => {
            var refreshToken = bondoraLinkedAccount.refreshToken;

            return new Promise((resolve, reject) => {
                request({
                    url: 'https://api.bondora.com/oauth/access_token',
                    method: 'post',
                    form: {
                        refresh_token: refreshToken,
                        grant_type: 'refresh_token',
                        client_id: config.oauth.bondora.clientId,
                        client_secret: config.oauth.bondora.clientSecret,
                        redirect_uri: config.oauth.bondora.callbackUrl,
                    },
                    json: true,
                }, (error, response, body) => {
                    if (error) {
                        return reject(error);
                    }

                    if (!body || body.error) {
                        return reject(body);
                    }

                    resolve(body);
                });
            });
        })
        .then((response) => {
            return AccessToken.findOneAndUpdate({
                type: 'bondora',
            }, {
                createdAt: Date.now(),
                type: 'bondora',
                value: response.access_token,
            }, {
                upsert: true,
                new: true,
            }).exec();
        })
        .then((accessToken) => accessToken.value);
}
