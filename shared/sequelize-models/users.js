/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'email'
    },
    encryptedPassword: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'encrypted_password'
    },
    resetPasswordToken: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'reset_password_token'
    },
    rememberCreatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'remember_created_at'
    },
    signInCount: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'sign_in_count'
    },
    currentSignInAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'current_sign_in_at'
    },
    lastSignInAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'last_sign_in_at'
    },
    currentSignInIp: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'current_sign_in_ip'
    },
    lastSignInIp: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'last_sign_in_ip'
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'name'
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'phone'
    },
    countryCode: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'country_code'
    },
    bio: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'bio'
    },
    receiveAnnouncements: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'receive_announcements'
    },
    avatarImageUid: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'avatar_image_uid'
    },
    authenticationToken: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'authentication_token'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'updatedAt'
    },
    resetPasswordSentAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'reset_password_sent_at'
    },
    authyId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'authy_id'
    },
    verified: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: "0",
      field: 'verified'
    }
  }, {
    tableName: 'users'
  });
};
