/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('freqList', {
    freqId: {
      type: DataTypes.INTEGER(10),
      allowNull: true,
      field: 'FreqId'
    },
    freqName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'FreqName'
    }
  }, {
    tableName: 'freq_list'
  });
};
