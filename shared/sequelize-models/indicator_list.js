/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('indicatorList', {
    indicatorId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'IndicatorID'
    },
    indicatorName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'IndicatorName'
    },
    metric: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Metric'
    },
    dataSource: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'DataSource'
    }
  }, {
    tableName: 'indicator_list'
  });
};
