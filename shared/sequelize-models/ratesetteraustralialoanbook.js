/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ratesetteraustralialoanbook', {
    contractId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'ContractID'
    },
    contractDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ContractDate'
    },
    contractTerm: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'ContractTerm'
    },
    annualRate: {
      type: "DOUBLE",
      allowNull: true,
      field: 'AnnualRate'
    },
    financeAmount: {
      type: "DOUBLE",
      allowNull: true,
      field: 'FinanceAmount'
    },
    principalOutstanding: {
      type: "DOUBLE",
      allowNull: true,
      field: 'PrincipalOutstanding'
    },
    financePurpose: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'FinancePurpose'
    },
    borrowerState: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'BorrowerState'
    },
    borrowerAge: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'BorrowerAge'
    },
    employmentStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentStatus'
    },
    borrowerIncome: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'BorrowerIncome'
    },
    housingStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'HousingStatus'
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Gender'
    },
    earlyPaymentsMade: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EarlyPaymentsMade'
    },
    repaymentStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'RepaymentStatus'
    }
  }, {
    tableName: 'ratesetteraustralialoanbook'
  });
};
