/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('countryList', {
    countryName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CountryName'
    },
    a2: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'A2'
    },
    a3: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'A3'
    },
    countryId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CountryID'
    }
  }, {
    tableName: 'country_list'
  });
};
