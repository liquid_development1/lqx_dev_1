/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bondoraPublicdataset', {
    loanId: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      field: 'LoanId'
    },
    loanNumber: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LoanNumber'
    },
    listedOnUtc: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ListedOnUTC'
    },
    biddingStartedOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'BiddingStartedOn'
    },
    bidsPortfolioManager: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'BidsPortfolioManager'
    },
    bidsApi: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'BidsApi'
    },
    bidsManual: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'BidsManual'
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'UserName'
    },
    newCreditCustomer: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'NewCreditCustomer'
    },
    loanApplicationStartedDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LoanApplicationStartedDate'
    },
    loanDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LoanDate'
    },
    contractEndDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ContractEndDate'
    },
    firstPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'FirstPaymentDate'
    },
    maturityDateOriginal: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'MaturityDate_Original'
    },
    maturityDateLast: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'MaturityDate_Last'
    },
    applicationSignedHour: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationSignedHour'
    },
    applicationSignedWeekday: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationSignedWeekday'
    },
    verificationType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'VerificationType'
    },
    languageCode: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LanguageCode'
    },
    age: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Age'
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DateOfBirth'
    },
    gender: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Gender'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Country'
    },
    county: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'County'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'City'
    },
    appliedAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AppliedAmount'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Amount'
    },
    interest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Interest'
    },
    loanDuration: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LoanDuration'
    },
    monthlyPayment: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'MonthlyPayment'
    },
    useOfLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'UseOfLoan'
    },
    education: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Education'
    },
    maritalStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'MaritalStatus'
    },
    nrOfDependants: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'NrOfDependants'
    },
    employmentStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'EmploymentStatus'
    },
    employmentDurationCurrentEmployer: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentDurationCurrentEmployer'
    },
    employmentPosition: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentPosition'
    },
    workExperience: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'WorkExperience'
    },
    occupationArea: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'OccupationArea'
    },
    homeOwnershipType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'HomeOwnershipType'
    },
    incomeFromPrincipalEmployer: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromPrincipalEmployer'
    },
    incomeFromPension: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromPension'
    },
    incomeFromFamilyAllowance: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromFamilyAllowance'
    },
    incomeFromSocialWelfare: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromSocialWelfare'
    },
    incomeFromLeavePay: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromLeavePay'
    },
    incomeFromChildSupport: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromChildSupport'
    },
    incomeOther: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeOther'
    },
    incomeTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeTotal'
    },
    existingLiabilities: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ExistingLiabilities'
    },
    refinanceLiabilities: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'RefinanceLiabilities'
    },
    liabilitiesTotal: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LiabilitiesTotal'
    },
    debtToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'DebtToIncome'
    },
    freeCash: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'FreeCash'
    },
    monthlyPaymentDay: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'MonthlyPaymentDay'
    },
    activeScheduleFirstPaymentReached: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'ActiveScheduleFirstPaymentReached'
    },
    plannedPrincipalTillDate: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PlannedPrincipalTillDate'
    },
    plannedInterestTillDate: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PlannedInterestTillDate'
    },
    lastPaymentOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LastPaymentOn'
    },
    currentDebtDaysPrimary: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'CurrentDebtDaysPrimary'
    },
    debtOccuredOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DebtOccuredOn'
    },
    currentDebtDaysSecondary: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'CurrentDebtDaysSecondary'
    },
    debtOccuredOnForSecondary: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DebtOccuredOnForSecondary'
    },
    expectedLoss: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'ExpectedLoss'
    },
    lossGivenDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'LossGivenDefault'
    },
    expectedReturn: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'ExpectedReturn'
    },
    probabilityOfDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'ProbabilityOfDefault'
    },
    defaultDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DefaultDate'
    },
    principalOverdueBySchedule: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalOverdueBySchedule'
    },
    plannedPrincipalPostDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PlannedPrincipalPostDefault'
    },
    plannedInterestPostDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PlannedInterestPostDefault'
    },
    ead1: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EAD1'
    },
    ead2: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EAD2'
    },
    principalRecovery: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalRecovery'
    },
    interestRecovery: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InterestRecovery'
    },
    recoveryStage: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'RecoveryStage'
    },
    stageActiveSince: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'StageActiveSince'
    },
    modelVersion: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ModelVersion'
    },
    rating: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Rating'
    },
    el: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EL'
    },
    ratingV0: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Rating_V0'
    },
    elV1: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EL_V1'
    },
    ratingV1: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Rating_V1'
    },
    elV2: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EL_V2'
    },
    ratingV2: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Rating_V2'
    },
    loanCancelled: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'LoanCancelled'
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Status'
    },
    restructured: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'Restructured'
    },
    activeLateCategory: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'ActiveLateCategory'
    },
    worseLateCategory: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'WorseLateCategory'
    },
    creditScoreEsMicroL: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEsMicroL'
    },
    creditScoreEsEquifaxRisk: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEsEquifaxRisk'
    },
    creditScoreFiAsiakasTietoRiskGrade: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreFiAsiakasTietoRiskGrade'
    },
    creditScoreEeMini: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEeMini'
    },
    principalPaymentsMade: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalPaymentsMade'
    },
    interestAndPenaltyPaymentsMade: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InterestAndPenaltyPaymentsMade'
    },
    principalWriteOffs: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalWriteOffs'
    },
    interestAndPenaltyWriteOffs: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InterestAndPenaltyWriteOffs'
    },
    principalDebtServicingCost: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalDebtServicingCost'
    },
    interestAndPenaltyDebtServicingCost: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InterestAndPenaltyDebtServicingCost'
    },
    principalBalance: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalBalance'
    },
    interestAndPenaltyBalance: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InterestAndPenaltyBalance'
    },
    noOfPreviousLoansBeforeLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NoOfPreviousLoansBeforeLoan'
    },
    amountOfPreviousLoansBeforeLoan: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AmountOfPreviousLoansBeforeLoan'
    },
    previousRepaymentsBeforeLoan: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PreviousRepaymentsBeforeLoan'
    },
    previousEarlyRepaymentsBeforeLoan: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PreviousEarlyRepaymentsBeforeLoan'
    },
    previousEarlyRepaymentsCountBeforeLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'PreviousEarlyRepaymentsCountBeforeLoan'
    },
    gracePeriodStart: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'GracePeriodStart'
    },
    gracePeriodEnd: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'GracePeriodEnd'
    },
    nextPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'NextPaymentDate'
    },
    nextPaymentNr: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NextPaymentNr'
    },
    nrOfScheduledPayments: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NrOfScheduledPayments'
    },
    reScheduledOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ReScheduledOn'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'bondora_publicdataset'
  });
};
