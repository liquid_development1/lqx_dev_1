/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('macro', {
    rowNames: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'row_names'
    },
    date: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Date'
    },
    country: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Country'
    },
    gdp: {
      type: "DOUBLE",
      allowNull: true,
      field: 'GDP'
    }
  }, {
    tableName: 'Macro'
  });
};
