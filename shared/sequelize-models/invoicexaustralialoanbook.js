/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('invoicexaustralialoanbook', {
    id: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'ID',
      primaryKey: true,
    },
    tradeDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Trade date:'
    },
    expectedPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Expected payment date'
    },
    settlementDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Settlement Date'
    },
    tradeStatus: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Trade status'
    },
    priceGrading: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Price grading'
    },
    faceValue: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Face value'
    },
    advance: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Advance'
    },
    advance1: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Advance1'
    },
    discountFee: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Discount fee'
    },
    annualisedYield: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Annualised yield'
    },
    cumulativeFaceValue: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Cumulative face value'
    },
    cumulativeAdvances: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Cumulative advances'
    },
    count: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Count'
    }
  }, {
    tableName: 'invoicexaustralialoanbook'
  });
};
