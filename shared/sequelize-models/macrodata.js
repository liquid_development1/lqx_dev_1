/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('macrodata', {
    date: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Date'
    },
    countryA3: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'CountryA3'
    },
    indicatorId: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'IndicatorID'
    },
    freqId: {
      type: "DOUBLE(53,0)",
      allowNull: true,
      field: 'FreqID'
    },
    amount: {
      type: "DOUBLE(53,0)",
      allowNull: true,
      field: 'Amount'
    }
  }, {
    tableName: 'macrodata'
  });
};
