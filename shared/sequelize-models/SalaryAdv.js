/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('salaryAdv', {
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Country'
    },
    year: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      field: 'Year'
    },
    semiYear: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      field: 'SemiYear'
    },
    minWage: {
      type: "DOUBLE",
      allowNull: true,
      field: 'MinWage'
    }
  }, {
    tableName: 'SalaryAdv'
  });
};
