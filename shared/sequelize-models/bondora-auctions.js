/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bondoraAuctions', {
    loanId: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      field: 'loanId'
    },
    auctionId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'auctionId'
    },
    loanNumber: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'loanNumber'
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'userName'
    },
    newCreditNumber: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'newCreditNumber'
    },
    loanApplicationStartedDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'loanApplicationStartedDate'
    },
    plannedCloseDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'plannedCloseDate'
    },
    applicationSignedHour: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'applicationSignedHour'
    },
    applicationSignedWeekday: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'applicationSignedWeekday'
    },
    verificationType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'verificationType'
    },
    languageCode: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'languageCode'
    },
    age: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'age'
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'dateOfBirth'
    },
    gender: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'gender'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'country'
    },
    creditScoreEsMicroL: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreEsMicroL'
    },
    creditScoreEsEquifaxRisk: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreEsEquifaxRisk'
    },
    creditScoreFiAsiakasTietoRiskGrade: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreFiAsiakasTietoRiskGrade'
    },
    creditScoreEeMini: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreEeMini'
    },
    appliedAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'appliedAmount'
    },
    interest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'interest'
    },
    loanDuration: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'loanDuration'
    },
    county: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'county'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'city'
    },
    useOfLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'useOfLoan'
    },
    education: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'education'
    },
    maritalStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'maritalStatus'
    },
    nrOfDependants: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'nrOfDependants'
    },
    employmentStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'employmentStatus'
    },
    employmentDurationCurrentEmployer: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employmentDurationCurrentEmployer'
    },
    employmentPosition: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employmentPosition'
    },
    workExperience: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'workExperience'
    },
    occupationArea: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'occupationArea'
    },
    homeOwnershipType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'homeOwnershipType'
    },
    incomeFromPrincipalEmployer: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeFromPrincipalEmployer'
    },
    incomeFromPension: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeFromPension'
    },
    incomeFromFamilyAllowance: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeFromFamilyAllowance'
    },
    incomeFromSocialWelfare: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeFromSocialWelfare'
    },
    incomeFromLeavePay: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeFromLeavePay'
    },
    incomeFromChildSupport: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeFromChildSupport'
    },
    incomeOther: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeOther'
    },
    incomeTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'incomeTotal'
    },
    freeCash: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'freeCash'
    },
    debtToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'debtToIncome'
    },
    monthlyPayment: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'monthlyPayment'
    },
    monthlyPaymentDay: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'monthlyPaymentDay'
    },
    modelVersion: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'modelVersion'
    },
    expectedLoss: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'expectedLoss'
    },
    rating: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'rating'
    },
    lossGivenDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'lossGivenDefault'
    },
    probabilityOfDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'probabilityOfDefault'
    },
    expectedReturnAlpha: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'expectedReturnAlpha'
    },
    liabilitiesTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'liabilitiesTotal'
    },
    listedOnUtc: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'listedOnUTC'
    },
    actualCloseDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'actualCloseDate'
    },
    winningBidsAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'winningBidsAmount'
    },
    remainingAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'remainingAmount'
    },
    userBids: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'userBids'
    },
    userBidAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'userBidAmount'
    },
    fullfilled: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'fullfilled'
    },
    creditScore: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'creditScore'
    },
    scoringDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'scoringDate'
    },
    eadRate: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'eadRate'
    },
    maturityFactor: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'maturityFactor'
    },
    interestRateAlpha: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'interestRateAlpha'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'bondora_auctions'
  });
};
