/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('exchangeRateMas', {
    endOfDay: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: "",
      primaryKey: true,
      field: 'end_of_day'
    },
    preliminary: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'preliminary'
    },
    eurSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'eur_sgd'
    },
    gbpSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'gbp_sgd'
    },
    usdSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'usd_sgd'
    },
    audSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'aud_sgd'
    },
    cadSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'cad_sgd'
    },
    cnySgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'cny_sgd_100'
    },
    hkdSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'hkd_sgd_100'
    },
    inrSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'inr_sgd_100'
    },
    idrSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'idr_sgd_100'
    },
    jpySgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'jpy_sgd_100'
    },
    krwSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'krw_sgd_100'
    },
    myrSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'myr_sgd_100'
    },
    twdSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'twd_sgd_100'
    },
    nzdSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'nzd_sgd'
    },
    phpSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'php_sgd_100'
    },
    qarSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'qar_sgd_100'
    },
    sarSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'sar_sgd_100'
    },
    chfSgd: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'chf_sgd'
    },
    thbSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'thb_sgd_100'
    },
    aedSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'aed_sgd_100'
    },
    vndSgd100: {
      type: DataTypes.FLOAT,
      allowNull: true,
      field: 'vnd_sgd_100'
    },
    feedsFlatstoreEntryId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'feeds_flatstore_entry_id'
    },
    timestamp: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'timestamp'
    },
    feedsEntityId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'feeds_entity_id'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'exchangeRateMAS'
  });
};
