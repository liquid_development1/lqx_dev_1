/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('scoringTable', {
    rowNames: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'row_names'
    },
    loanNumber: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'LoanNumber'
    },
    totalScore: {
      type: "DOUBLE",
      allowNull: true,
      field: 'TotalScore'
    },
    pd: {
      type: "DOUBLE",
      allowNull: true,
      field: 'PD'
    },
    scoreGr: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'ScoreGR'
    },
    creditRating: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'CreditRating'
    }
  }, {
    tableName: 'scoringTable'
  });
};
