/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('salary', {
    currency: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Currency'
    },
    country: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Country'
    },
    period: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'Period'
    },
    minWage: {
      type: "DOUBLE",
      allowNull: true,
      field: 'MinWage'
    }
  }, {
    tableName: 'salary'
  });
};
