/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('lendingworkloandata', {
    loanId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'Loan ID'
    },
    borrowerId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'Borrower ID'
    },
    startDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Start Date'
    },
    term: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Term'
    },
    maturityDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Maturity Date'
    },
    security: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Security'
    },
    shieldCoverage: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Shield Coverage'
    },
    loanPurpose: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Loan Purpose'
    },
    loanStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Loan Status'
    },
    repaymentStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Repayment Status'
    },
    repaymentType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Repayment Type'
    },
    amount: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Amount'
    },
    grossRate: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Gross Rate'
    },
    principalOutstanding: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Principal Outstanding'
    }
  }, {
    tableName: 'lendingworkloandata'
  });
};
