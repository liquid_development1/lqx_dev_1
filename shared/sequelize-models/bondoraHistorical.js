/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bondoraHistorical', {
    idHistorical: {
      type: DataTypes.INTEGER(255),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'Id_Historical'
    },
    reportAsOfEod: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ReportAsOfEOD'
    },
    loanId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'LoanId'
    },
    loanNumber: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'LoanNumber'
    },
    creditDecision: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'CreditDecision'
    },
    wasFunded: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'WasFunded'
    },
    bidsInvestmentPlan: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'BidsInvestmentPlan'
    },
    bidsApi: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'BidsApi'
    },
    bidsManual: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'BidsManual'
    },
    isBusinessLoan: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'IsBusinessLoan'
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'UserName'
    },
    newCreditCustomer: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'NewCreditCustomer'
    },
    loanApplicationStartedDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LoanApplicationStartedDate'
    },
    loanDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LoanDate'
    },
    contractEndDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ContractEndDate'
    },
    firstPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'FirstPaymentDate'
    },
    maturityDateOriginal: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'MaturityDate_Original'
    },
    maturityDateLast: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'MaturityDate_Last'
    },
    has1DPassedFromFirstPayment: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'Has1DPassedFromFirstPayment'
    },
    has14DPassedFromFirstPayment: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'Has14DPassedFromFirstPayment'
    },
    has30DPassedFromFirstPayment: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'Has30DPassedFromFirstPayment'
    },
    has60DPassedFromFirstPayment: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'Has60DPassedFromFirstPayment'
    },
    applicationSignedHour: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationSignedHour'
    },
    applicationSignedWeekday: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationSignedWeekday'
    },
    verificationType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'VerificationType'
    },
    languageCode: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LanguageCode'
    },
    age: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Age'
    },
    gender: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Gender'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Country'
    },
    county: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'County'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'City'
    },
    creditScore: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScore'
    },
    totalNumDebts: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'TotalNumDebts'
    },
    totalMaxDebtMonths: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'TotalMaxDebtMonths'
    },
    numDebtsFinance: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'NumDebtsFinance'
    },
    maxDebtMonthsFinance: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'MaxDebtMonthsFinance'
    },
    numDebtsTelco: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'NumDebtsTelco'
    },
    maxDebtMonthsTelco: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'MaxDebtMonthsTelco'
    },
    numDebtsOther: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'NumDebtsOther'
    },
    maxDebtMonthsOther: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'MaxDebtMonthsOther'
    },
    appliedAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AppliedAmount'
    },
    fundedAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'FundedAmount'
    },
    interest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Interest'
    },
    issuedInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IssuedInterest'
    },
    loanDuration: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LoanDuration'
    },
    useOfLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'UseOfLoan'
    },
    applicationType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationType'
    },
    education: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Education'
    },
    maritalStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'MaritalStatus'
    },
    nrOfDependants: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NrOfDependants'
    },
    employmentStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'EmploymentStatus'
    },
    employmentDurationCurrentEmployer: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentDurationCurrentEmployer'
    },
    employmentPosition: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentPosition'
    },
    workExperience: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'WorkExperience'
    },
    occupationArea: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'OccupationArea'
    },
    homeOwnershipType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'HomeOwnershipType'
    },
    incomeFromPrincipalEmployer: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromPrincipalEmployer'
    },
    incomeFromPension: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromPension'
    },
    incomeFromFamilyAllowance: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromFamilyAllowance'
    },
    incomeFromSocialWelfare: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromSocialWelfare'
    },
    incomeFromLeavePay: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromLeavePay'
    },
    incomeFromChildSupport: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromChildSupport'
    },
    incomeOther: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeOther'
    },
    incomeTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeTotal'
    },
    totalLiabilitiesBeforeLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'TotalLiabilitiesBeforeLoan'
    },
    debtLiabilitiesBeforeLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'DebtLiabilitiesBeforeLoan'
    },
    otherLiabilitiesBeforeLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'OtherLiabilitiesBeforeLoan'
    },
    totalMonthlyLiabilities: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'TotalMonthlyLiabilities'
    },
    debtToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'DebtToIncome'
    },
    freeCash: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'FreeCash'
    },
    newLoanMonthlyPayment: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'NewLoanMonthlyPayment'
    },
    appliedAmountToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AppliedAmountToIncome'
    },
    liabilitiesToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'LiabilitiesToIncome'
    },
    newPaymentToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'NewPaymentToIncome'
    },
    countOfBankCredits: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'CountOfBankCredits'
    },
    sumOfBankCredits: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'SumOfBankCredits'
    },
    countOfPaydayLoans: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'CountOfPaydayLoans'
    },
    sumOfPaydayLoans: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'SumOfPaydayLoans'
    },
    countOfOtherCredits: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'CountOfOtherCredits'
    },
    sumOfOtherCredits: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'SumOfOtherCredits'
    },
    noOfPreviousApplications: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NoOfPreviousApplications'
    },
    amountOfPreviousApplications: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AmountOfPreviousApplications'
    },
    noOfPreviousLoans: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NoOfPreviousLoans'
    },
    amountOfPreviousLoans: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AmountOfPreviousLoans'
    },
    noOfInvestments: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NoOfInvestments'
    },
    amountOfInvestments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AmountOfInvestments'
    },
    noOfBids: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NoOfBids'
    },
    amountOfBids: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AmountOfBids'
    },
    previousRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PreviousRepayments'
    },
    previousLateFeesPaid: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PreviousLateFeesPaid'
    },
    previousEarlyRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PreviousEarlyRepayments'
    },
    hasNewSchedule: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'HasNewSchedule'
    },
    bondoraCreditHistory: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'BondoraCreditHistory'
    },
    newOfferMade: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'NewOfferMade'
    },
    monthlyPaymentDay: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'MonthlyPaymentDay'
    },
    lastPaymentOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LastPaymentOn'
    },
    outstandingPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'OutstandingPrincipal'
    },
    unpaidInterestOutstanding: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'UnpaidInterestOutstanding'
    },
    interestAndPenaltiesPaid: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InterestAndPenaltiesPaid'
    },
    cancelledWithin14Days: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'CancelledWithin14Days'
    },
    gracePeriodStart: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'GracePeriodStart'
    },
    gracePeriodEnd: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'GracePeriodEnd'
    },
    currentLoanHasBeenExtended: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'CurrentLoanHasBeenExtended'
    },
    inDebt1Day: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'InDebt1Day'
    },
    inDebt14Day: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'InDebt14Day'
    },
    inDebt30Day: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'InDebt30Day'
    },
    inDebt60Day: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'InDebt60Day'
    },
    isFirstPaymentDefault: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'IsFirstPaymentDefault'
    },
    leftMoneyForFirstPayment: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'LeftMoneyForFirstPayment'
    },
    currentDebtDays: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'CurrentDebtDays'
    },
    principalDebtAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalDebtAmount'
    },
    inDebt1DayStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'InDebt1Day_StartDate'
    },
    inDebt1DayPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt1Day_Principal'
    },
    inDebt1DayPrincipalProportion: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt1Day_PrincipalProportion'
    },
    inDebt1DayInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt1Day_Interest'
    },
    inDebt1DayExposure: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt1Day_Exposure'
    },
    inDebt1DayTotalRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt1Day_TotalRepayments'
    },
    inDebt7DayStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'InDebt7Day_StartDate'
    },
    inDebt7DayPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt7Day_Principal'
    },
    inDebt7DayPrincipalProportion: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt7Day_PrincipalProportion'
    },
    inDebt7DayInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt7Day_Interest'
    },
    inDebt7DayExposure: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt7Day_Exposure'
    },
    inDebt7DayTotalRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt7Day_TotalRepayments'
    },
    inDebt14DayStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'InDebt14Day_StartDate'
    },
    inDebt14DayPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt14Day_Principal'
    },
    inDebt14DayPrincipalProportion: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt14Day_PrincipalProportion'
    },
    inDebt14DayInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt14Day_Interest'
    },
    inDebt14DayExposure: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt14Day_Exposure'
    },
    inDebt14DayTotalRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt14Day_TotalRepayments'
    },
    inDebt21DayStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'InDebt21Day_StartDate'
    },
    inDebt21DayPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt21Day_Principal'
    },
    inDebt21DayPrincipalProportion: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt21Day_PrincipalProportion'
    },
    inDebt21DayInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt21Day_Interest'
    },
    inDebt21DayExposure: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt21Day_Exposure'
    },
    inDebt21DayTotalRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt21Day_TotalRepayments'
    },
    inDebt30DayStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'InDebt30Day_StartDate'
    },
    inDebt30DayPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt30Day_Principal'
    },
    inDebt30DayPrincipalProportion: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt30Day_PrincipalProportion'
    },
    inDebt30DayInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt30Day_Interest'
    },
    inDebt30DayExposure: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt30Day_Exposure'
    },
    inDebt30DayTotalRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt30Day_TotalRepayments'
    },
    inDebt60DayStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'InDebt60Day_StartDate'
    },
    inDebt60DayPrincipal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt60Day_Principal'
    },
    inDebt60DayPrincipalProportion: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt60Day_PrincipalProportion'
    },
    inDebt60DayInterest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt60Day_Interest'
    },
    inDebt60DayExposure: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt60Day_Exposure'
    },
    inDebt60DayTotalRepayments: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'InDebt60Day_TotalRepayments'
    },
    debtRestructuringDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DebtRestructuringDate'
    },
    principalAtDebtRestructuring: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Principal_at_DebtRestructuring'
    },
    principalProportionAtDebtRestructuring: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalProportion_at_DebtRestructuring'
    },
    interestAtDebtRestructuring: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Interest_at_DebtRestructuring'
    },
    exposureAtDebtRestructuring: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Exposure_at_DebtRestructuring'
    },
    totalRepaymentsAtDebtRestructuring: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'TotalRepayments_at_DebtRestructuring'
    },
    defaultStartDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Default_StartDate'
    },
    principalAtDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Principal_at_Default'
    },
    principalProportionAtDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'PrincipalProportion_at_Default'
    },
    interestAtDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Interest_at_Default'
    },
    exposureAtDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Exposure_at_Default'
    },
    totalRepaymentsAtDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'TotalRepayments_at_Default'
    },
    defaultedOnDay: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DefaultedOnDay'
    },
    ad: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'AD'
    },
    ead1: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EAD1'
    },
    ead2: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EAD2'
    },
    recovery: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Recovery'
    },
    scoringDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ScoringDate'
    },
    modelVersion: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ModelVersion'
    },
    el: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'EL'
    },
    rating: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Rating'
    },
    creditScoreEsMicroL: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEsMicroL'
    },
    creditScoreEsEquifaxRisk: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEsEquifaxRisk'
    },
    creditScoreFiAsiakasTietoRiskGrade: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreFiAsiakasTietoRiskGrade'
    },
    creditScoreEeMini: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEeMini'
    },
    isInactiveDuplicate: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'IsInactiveDuplicate'
    },
    associatedDuplicateLoanId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'AssociatedDuplicateLoanId'
    },
    cancelledWithin1Month: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'CancelledWithin1Month'
    },
    earlyRepaidWithin14Days: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'EarlyRepaidWithin14Days'
    },
    postFundingCancellation: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'PostFundingCancellation'
    },
    idCancellation: {
      type: DataTypes.INTEGER(1),
      allowNull: true,
      field: 'IdCancellation'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'bondoraHistorical'
  });
};
