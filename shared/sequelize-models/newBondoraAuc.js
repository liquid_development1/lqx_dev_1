/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('newBondoraAuc', {
    id: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'Id'
    },
    loanId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'LoanId'
    },
    auctionId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'AuctionId'
    },
    loanNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'LoanNumber'
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'UserName'
    },
    newCreditCustomer: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NewCreditCustomer'
    },
    loanApplicationStartedDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'LoanApplicationStartedDate'
    },
    applicationSignedHour: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationSignedHour'
    },
    applicationSignedWeekday: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ApplicationSignedWeekday'
    },
    verificationType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'VerificationType'
    },
    languageCode: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LanguageCode'
    },
    age: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Age'
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'DateOfBirth'
    },
    gender: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Gender'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Country'
    },
    creditScoreEsMicroL: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEsMicroL'
    },
    creditScoreEsEquifaxRisk: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreEsEquifaxRisk'
    },
    creditScoreFiAsiakasTietoRiskGrade: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'CreditScoreFiAsiakasTietoRiskGrade'
    },
    creditScoreEeMini: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'CreditScoreEeMini'
    },
    appliedAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'AppliedAmount'
    },
    interest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'Interest'
    },
    loanDuration: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'LoanDuration'
    },
    county: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'County'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'City'
    },
    useOfLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'UseOfLoan'
    },
    education: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'Education'
    },
    maritalStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'MaritalStatus'
    },
    nrOfDependants: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'NrOfDependants'
    },
    employmentStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'EmploymentStatus'
    },
    employmentDurationCurrentEmployer: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentDurationCurrentEmployer'
    },
    employmentPosition: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentPosition'
    },
    workExperience: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'WorkExperience'
    },
    occupationArea: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'OccupationArea'
    },
    homeOwnershipType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'HomeOwnershipType'
    },
    incomeFromPrincipalEmployer: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromPrincipalEmployer'
    },
    incomeFromPension: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromPension'
    },
    incomeFromFamilyAllowance: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromFamilyAllowance'
    },
    incomeFromSocialWelfare: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromSocialWelfare'
    },
    incomeFromLeavePay: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromLeavePay'
    },
    incomeFromChildSupport: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeFromChildSupport'
    },
    incomeOther: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeOther'
    },
    incomeTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'IncomeTotal'
    },
    freeCash: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'FreeCash'
    },
    debtToIncome: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'DebtToIncome'
    },
    monthlyPayment: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'MonthlyPayment'
    },
    monthlyPaymentDay: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'MonthlyPaymentDay'
    },
    modelVersion: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'ModelVersion'
    },
    expectedLoss: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'ExpectedLoss'
    },
    rating: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Rating'
    },
    bondoraCreditHistory: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'BondoraCreditHistory'
    },
    lossGivenDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'LossGivenDefault'
    },
    probabilityOfDefault: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'ProbabilityOfDefault'
    },
    expectedReturnAlpha: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'ExpectedReturnAlpha'
    },
    liabilitiesTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'LiabilitiesTotal'
    },
    listedOnUtc: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ListedOnUTC'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'newBondoraAuc'
  });
};
