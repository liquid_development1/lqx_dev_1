/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bondoraBalance', {
    userId: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      field: 'userId',
      defaultValue: 0,
    },
    balance: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      defaultValue: "0.000",
      field: 'balance'
    }
  }, {
    tableName: 'bondora_balance',
  });
};
