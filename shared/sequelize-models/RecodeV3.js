/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('recodeV3', {
    loanNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'LoanNumber'
    },
    employmentPositionRec: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'EmploymentPositionRec'
    }
  }, {
    tableName: 'RecodeV3'
  });
};
