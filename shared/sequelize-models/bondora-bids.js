/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bondoraBids', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    auctionId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'auctionId'
    },
    bidValue: {
      type: DataTypes.DECIMAL,
      allowNull: false,
      field: 'bidValue'
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'users',
        key: 'id'
      },
      field: 'userId'
    }
  }, {
    tableName: 'bondora_bids'
  });
};
