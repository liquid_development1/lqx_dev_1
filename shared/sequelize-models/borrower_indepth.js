/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('borrowerIndepth', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'email'
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'firstName'
    },
    middleName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'middleName'
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'lastName'
    },
    dateofBirth: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'dateofBirth'
    },
    gender: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'gender'
    },
    martialStatus: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'martialStatus'
    },
    noDependents: {
      type: DataTypes.INTEGER(2),
      allowNull: false,
      field: 'noDependents'
    },
    idType: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'idType'
    },
    idNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'idNumber'
    },
    idIssueDate: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'idIssueDate'
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'address'
    },
    city: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'city'
    },
    state: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'state'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'country'
    },
    zipCode: {
      type: DataTypes.INTEGER(200),
      allowNull: false,
      field: 'zipCode'
    },
    citizenship: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'citizenship'
    },
    yearsOfResidence: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'yearsOfResidence'
    },
    mobileNumber: {
      type: DataTypes.INTEGER(9),
      allowNull: true,
      field: 'mobileNumber'
    },
    nativeLanguage: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'nativeLanguage'
    },
    foreignLanguage: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'foreignLanguage'
    },
    educationLevel: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'educationLevel'
    },
    educationCertificates: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'educationCertificates'
    },
    employmentStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employmentStatus'
    },
    workExperience: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'workExperience'
    },
    employer: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employer'
    },
    industry: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'industry'
    },
    jobPosition: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'jobPosition'
    },
    employerAddress: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerAddress'
    },
    employerCity: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerCity'
    },
    employerState: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerState'
    },
    employerCountry: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerCountry'
    },
    employerContact: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerContact'
    },
    employerWebSite: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerWebSite'
    },
    employerEmail: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'employerEmail'
    },
    yearsOfEmployment: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'yearsOfEmployment'
    },
    homeOwnership: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'homeOwnership'
    },
    homeType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'homeType'
    },
    vehicleOwnership: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'vehicleOwnership'
    },
    vehicleType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'vehicleType'
    },
    driverLicenseId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'driverLicenseId'
    },
    driverLicenseCategory: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'driverLicenseCategory'
    },
    drivingExperience: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'drivingExperience'
    },
    salary: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'salary'
    },
    otherIncome: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'otherIncome'
    },
    totalDebts: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'totalDebts'
    },
    noDebts: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'noDebts'
    },
    creditorType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditorType'
    },
    creditorName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditorName'
    },
    livingCosts: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'livingCosts'
    },
    otherLiabilities: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'otherLiabilities'
    },
    loanAmount: {
      type: DataTypes.INTEGER(45),
      allowNull: true,
      field: 'loanAmount'
    },
    loanCurrency: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'loanCurrency'
    },
    loanDuration: {
      type: DataTypes.INTEGER(10),
      allowNull: true,
      field: 'loanDuration'
    },
    loanPurpose: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'loanPurpose'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      field: 'updatedAt'
    }
  }, {
    tableName: 'borrower_indepth'
  });
};
