/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('zopaloanbook', {
    reportDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'ReportDate'
    },
    platformId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'PlatformId'
    },
    platform: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Platform'
    },
    altFiLoanId: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'AltFiLoanId'
    },
    invoiceAmount: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'InvoiceAmount'
    },
    loanAmount: {
      type: "DOUBLE",
      allowNull: true,
      field: 'LoanAmount'
    },
    issueDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'IssueDate'
    },
    term: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'Term'
    },
    interestRate: {
      type: "DOUBLE",
      allowNull: true,
      field: 'InterestRate'
    },
    loanStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'LoanStatus'
    },
    totalInterestPaid: {
      type: "DOUBLE",
      allowNull: true,
      field: 'TotalInterestPaid'
    },
    totalRecovery: {
      type: "DOUBLE",
      allowNull: true,
      field: 'TotalRecovery'
    },
    totalChargedOffPrincipal: {
      type: "DOUBLE",
      allowNull: true,
      field: 'TotalChargedOffPrincipal'
    },
    totalPrincipalPaid: {
      type: "DOUBLE",
      allowNull: true,
      field: 'TotalPrincipalPaid'
    },
    principalOutstandingUsingCashFlows: {
      type: "DOUBLE",
      allowNull: true,
      field: 'PrincipalOutstanding_usingCashFlows'
    },
    loanAmountGroup: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'LoanAmountGroup'
    },
    originationQuarter: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'OriginationQuarter'
    },
    defaulted: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'Defaulted'
    },
    contingency: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'Contingency'
    },
    borrowerType: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'BorrowerType'
    },
    secured: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Secured'
    }
  }, {
    tableName: 'zopaloanbook'
  });
};
