/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('thincatsloandata', {
    loanId: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Loan ID'
    },
    borrowerId: {
      type: "BLOB",
      allowNull: true,
      field: 'Borrower ID'
    },
    loanCommencementDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'Loan Commencement Date'
    },
    amount: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Amount'
    },
    averageInterestRate: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Average Interest Rate'
    },
    loanTermMonths: {
      type: "DOUBLE",
      allowNull: true,
      field: 'Loan Term (months)'
    },
    security: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Security'
    },
    useOfFunds: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Use of Funds'
    },
    industrySector: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Industry sector'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Country'
    },
    loanStatus: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Loan Status'
    },
    repaymentTypeInterestCapital: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Repayment Type Interest & Capital'
    },
    repaymentTypeInterestOnly: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Repayment Type Interest Only'
    },
    repaymentTypeInterestRollup: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'Repayment Type Interest Rollup'
    }
  }, {
    tableName: 'thincatsloandata'
  });
};
