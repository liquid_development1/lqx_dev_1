/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('bondoraInvestments', {
    loanPartId: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      field: 'loanPartId'
    },
    amount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'amount'
    },
    auctionId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'auctionId'
    },
    auctionName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'auctionName'
    },
    auctionNumber: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'auctionNumber'
    },
    auctionBidNumber: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'auctionBidNumber'
    },
    auctionBidType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'auctionBidType'
    },
    country: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'country'
    },
    creditScore: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'creditScore'
    },
    creditScoreEsMicroL: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreEsMicroL'
    },
    creditScoreEsEquifaxRisk: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreEsEquifaxRisk'
    },
    creditScoreFiAsiakasTietoRiskGrade: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreFiAsiakasTietoRiskGrade'
    },
    creditScoreEeMini: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'creditScoreEeMini'
    },
    rating: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'rating'
    },
    interest: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'interest'
    },
    useOfLoan: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'useOfLoan'
    },
    incomeVerificationStatus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'incomeVerificationStatus'
    },
    loanId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'loanId'
    },
    loanStatusCode: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'loanStatusCode'
    },
    loanStatusActiveFrom: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'loanStatusActiveFrom'
    },
    userName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'userName'
    },
    gender: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'gender'
    },
    dateOfBirth: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'dateOfBirth'
    },
    signedDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'signedDate'
    },
    reScheduledOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'reScheduledOn'
    },
    debtOccurredOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'debtOccurredOn'
    },
    debtOccurredOnForSecondary: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'debtOccurredOnForSecondary'
    },
    loanDuration: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'loanDuration'
    },
    nextPaymentNr: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'nextPaymentNr'
    },
    nextPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'nextPaymentDate'
    },
    nextPaymentSum: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'nextPaymentSum'
    },
    nrOfScheduledPayments: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'nrOfScheduledPayments'
    },
    lastPaymentDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'lastPaymentDate'
    },
    principalRepaid: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'principalRepaid'
    },
    interestRepaid: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'interestRepaid'
    },
    lateAmountPaid: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'lateAmountPaid'
    },
    principalRemaining: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'principalRemaining'
    },
    principalLateAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'principalLateAmount'
    },
    interestLateAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'interestLateAmount'
    },
    penaltyLateAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'penaltyLateAmount'
    },
    lateAmountTotal: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'lateAmountTotal'
    },
    principalWriteOffAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'principalWriteOffAmount'
    },
    interestWriteOffAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'interestWriteOffAmount'
    },
    penaltyWriteOffAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'penaltyWriteOffAmount'
    },
    debtServicingCostMainAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'debtServicingCostMainAmount'
    },
    debtServicingCostInterestAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'debtServicingCostInterestAmount'
    },
    debtServicingCostPenaltyAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'debtServicingCostPenaltyAmount'
    },
    purchaseDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'purchaseDate'
    },
    soldDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'soldDate'
    },
    purchasePrice: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'purchasePrice'
    },
    salePrice: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'salePrice'
    },
    listedInSecondMarketOn: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'listedInSecondMarketOn'
    },
    latestDebtManagementStage: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'latestDebtManagementStage'
    },
    latestDebtManagementStageType: {
      type: DataTypes.BIGINT,
      allowNull: true,
      field: 'latestDebtManagementStageType'
    },
    latestDebtManagementDate: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'latestDebtManagementDate'
    },
    noteLoanTransfersMainAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'noteLoanTransfersMainAmount'
    },
    noteLoanTransfersInterestAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'noteLoanTransfersInterestAmount'
    },
    noteLoanLateChargesPaid: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'noteLoanLateChargesPaid'
    },
    noteLoanTransfersEarningsAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'noteLoanTransfersEarningsAmount'
    },
    noteLoanTransfersTotalRepaimentsAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true,
      field: 'noteLoanTransfersTotalRepaimentsAmount'
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'createdAt'
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'updatedAt'
    }
  }, {
    tableName: 'bondora_investments'
  });
};
