'use strict';

const amqplib = require('amqplib');

function Client(connectionString) {
    this._connectionString = connectionString;
    this._established = false;
    this._promise = null;
}

Client.prototype._connect = function(reconnect) {
    this._promise = amqplib
        .connect(this._connectionString)
        .then((conn) => {
            console.log('MQ Connected');

            this._established = true;
            this._connection = conn;

            conn.on('close', () => {
                console.log('MQ connection closed');
                this._connection = null;

                // reconnect
                this._connect();
            });

            return conn;
        })
        .catch((e) => {
            console.log('attempt to re-connect after 10 seconds...', e);

            setTimeout(this._connect.bind(this, true), 10e3);
        });

    return this._promise;
};

Client.prototype.getConnection = function() {
    if (this._established) {
        return Promise.resolve(this._connection);
    }

    if (this._promise) {
        return Promise.resolve(this._promise);
    }

    return this._connect();
};

module.exports.Client = Client;
