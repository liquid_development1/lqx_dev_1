CREATE TABLE `liquidityex_schema`.`bondora_balance` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userId` INT NULL DEFAULT NULL,
  `balance` DECIMAL(11,3) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`));

ALTER TABLE `liquidityex_schema`.`bondora_balance`
DROP COLUMN `id`,
CHANGE COLUMN `userId` `userId` INT(11) NOT NULL DEFAULT 0 ,
ADD COLUMN `createdAt` DATETIME NULL AFTER `balance`,
ADD COLUMN `updatedAt` DATETIME NULL AFTER `createdAt`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`userId`);
