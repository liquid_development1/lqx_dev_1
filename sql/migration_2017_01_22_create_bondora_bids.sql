CREATE TABLE `liquidityex_schema`.`bondora_bids` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `auctionId` VARCHAR(45) NOT NULL,
  `bidValue` DECIMAL(10, 5) NOT NULL,
  `userId` INT(11) NOT NULL,
  PRIMARY KEY (`id`));

ALTER TABLE `liquidityex_schema`.`bondora_bids`
ADD INDEX `userId_idx` (`userId` ASC);
ALTER TABLE `liquidityex_schema`.`bondora_bids`
ADD CONSTRAINT `userId`
  FOREIGN KEY (`userId`)
  REFERENCES `liquidityex_schema`.`users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `liquidityex_schema`.`bondora_bids`
ADD COLUMN `createdAt` DATETIME NULL AFTER `userId`,
ADD COLUMN `updatedAt` DATETIME NULL AFTER `createdAt`;
