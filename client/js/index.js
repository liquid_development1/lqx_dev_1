;(function () {
    'use strict';

    angular
        .module('singboapp', [
            'ui.router',
            'ui.select',
            'ngFlash',
            'ngResource',
            'ngSanitize',
            'flux',
        ])
        // import library
        .constant('_', window._)
        .constant('$', window.jQuery)
        .constant('d3', window.d3);
})();
