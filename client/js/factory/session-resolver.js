;(function() {
    'use strict';

    angular
        .module('singboapp')
        .constant('SessionResolver', SessionResolver)
        .constant('UnverifiedSessionResolver', UnverifiedSessionResolver);

    SessionResolver.$inject = ['$q', 'AuthService'];

    function SessionResolver($q, AuthService) {
        return AuthService
            .checkLogin()
            .catch(function(res) {
                var session = res && res.data;

                if (session && !session.verified) {
                    return $q.reject('Session Unverified');
                }

                return $q.reject('Unauthorized');
            });
    }

    UnverifiedSessionResolver.$inject = ['$q', 'AuthService', 'Session'];

    function UnverifiedSessionResolver($q, AuthService, Session) {
        return AuthService
            .checkLogin()
            .catch(function(res) {
                var session = res && res.data;

                if (session && !session.verified) {
                    return $q.resolve(new Session(session));
                }

                return $q.reject('Unauthorized');
            });
    }
})();
