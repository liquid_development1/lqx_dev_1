;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('AuthService', AuthService);

    var LOGGED_IN_KEY = btoa('LOGGED_IN');

    AuthService.$inject = ['$q', 'Session', 'User', 'StorageService'];

    function AuthService($q, Session, User, StorageService) {
        return {
            checkLogin: checkLogin,
            login: login,
            logout: logout,
            register: register,
            verifyToken: verifyToken,
        };

        function checkLogin() {
            var session = StorageService.get(LOGGED_IN_KEY);

            if (!session) {
                return $q.reject();
            }

            return Session.get({
                sid: session.sid,
            }).$promise;
        }

        function login(loginForm) {
            return Session.save({
                email: loginForm.email,
                password: loginForm.password,
            }).$promise.then(function(session) {
                StorageService.set(LOGGED_IN_KEY, session);

                return session;
            });
        }

        function logout() {
            var session = StorageService.get(LOGGED_IN_KEY);

            if (!session) {
                return $q.resolve();
            }

            return Session.delete({
                sid: session.sid,
            }).$promise.finally(function() {
                StorageService.remove(LOGGED_IN_KEY);
            });
        }

        function register(registrationForm) {
            return User.save(registrationForm).$promise;
        }

        function verifyToken(session) {
            return Session.verifyToken({
                sid: session.sid,
                token: session.token,
            }).$promise;
        }
    }
        // // create user variable
        // var user = null;
        // // return available functions for use in the controllers
        // return ({
        //     isLoggedIn: isLoggedIn,
        //     getUserStatus: getUserStatus,
        //     login: login,
        //     logout: logout,
        //     register: register
        // });

        //     function isLoggedIn() {
        //         if(user) {
        //             return true;
        //         } else {
        //             return false;
        //         }
        //     }

        //     function getUserStatus(callback) {
        //         $http.get('/status/user')
        //         // handle success
        //             .then(function (data) {
        //                 var authResult = JSON.stringify(data);
        //                 if(data["data"] != ''){
        //                     user = true;
        //                     callback(user);
        //                 } else {
        //                     user = false;
        //                     callback(user);
        //                 }
        //             });
        //     }

        //     function login(userProfile) {

        //         // create a new instance of deferred
        //         var deferred = $q.defer();

        //         // send a post request to the server
        //         $http.post('/login',
        //             userProfile)
        //         // handle success
        //             .then(function (data, status) {
        //                 if(data.status == 200){
        //                     getUserStatus(function(result){
        //                         if(result){
        //                             deferred.resolve();
        //                             $state.go('defaultApp');
        //                         }else{
        //                             deferred.reject();
        //                             Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
        //                             $state.go('SignIn');
        //                         }
        //                     });
        //                 } else {
        //                     user = false;
        //                     Flash.clear();
        //                     Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
        //                     deferred.reject();
        //                 }
        //             })
        //             // handle error
        //             .catch(function (data) {
        //                 user = false;
        //                 Flash.clear();
        //                 Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
        //                 deferred.reject();
        //             });

        //         // return promise object
        //         return deferred.promise;

        //     }

        //     function logout() {

        //         // create a new instance of deferred
        //         var deferred = $q.defer();

        //         // send a get request to the server
        //         $http.get('/logout')
        //         // handle success
        //             .then(function (data) {
        //                 user = false;
        //                 deferred.resolve();
        //             })
        //             // handle error
        //             .catch(function (data) {
        //                 user = false;
        //                 deferred.reject();
        //             });

        //         // return promise object
        //         return deferred.promise;

        //     }

        //     function register(username, password) {

        //         // create a new instance of deferred
        //         var deferred = $q.defer();

        //         // send a post request to the server
        //         $http.post('/register',
        //             {username: username, password: password})
        //         // handle success
        //             .then(function (data, status) {
        //                 if(status){
        //                     deferred.resolve();
        //                 } else {
        //                     Flash.closeFlash();
        //                     Flash.create('danger', "Can't register with us!", 0, {class: 'custom-class', id: 'custom-id'}, true);
        //                     deferred.reject();
        //                 }
        //             })
        //             // handle error
        //             .catch(function (data) {
        //                 Flash.clear();
        //                 Flash.create('danger', "Ooops something went wrong!", 0, {class: 'custom-class', id: 'custom-id'}, true);
        //                 deferred.reject();
        //             });

        //         // return promise object
        //         return deferred.promise;

        //     }

        // }]);

})();
