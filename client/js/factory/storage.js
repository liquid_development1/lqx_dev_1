;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('StorageService', StorageService);

    StorageService.$inject = ['$window'];

    function StorageService($window) {
        if ($window.localStorage) {
            return useLocalStorage($window);
        }

        return useCookie($window);
    }

    function useLocalStorage($window) {
        var localStorage = $window.localStorage;

        return {
            get: function(key) {
                var encoded = localStorage.getItem(key);

                try {
                    return JSON.parse(atob(encoded));
                } catch(e) {
                    return null;
                }
            },
            set: function(key, value) {
                var encoded = btoa(JSON.stringify(value));

                localStorage.setItem(key, encoded);
            },
            remove: localStorage.removeItem.bind(localStorage),
            clear: localStorage.clear.bind(localStorage),
        };
    }

    function useCookie($window) {
        // TODO: implement later
        return {};
    }
})();
