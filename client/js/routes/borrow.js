;(function() {
    'use strict';

    angular
        .module('singboapp')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                // Borrow
                .state('Borrow', {
                    url: '/borrow',
                    views: {
                        nav: {
                            templateUrl: 'views/navbar_2.html',
                        },
                        content: {
                            templateUrl: 'views/borrow.html',
                        },
                    },
                })
                // Borrow-Register
                .state('BorrowRegister', {
                    url: '/borrow-register',
                    views: {
                        content: {
                            templateUrl: 'views/register_borrow.html',
                            controller: 'RegisterCtrl2 as ctrl',
                        },
                    },
                });
        }]);
})();
