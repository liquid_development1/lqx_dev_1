;(function() {
    'use strict';

    angular
        .module('singboapp')
        .config(config);

    config.$inject = ['$stateProvider'];

    function config($stateProvider) {
        $stateProvider
            .state('LinkedAccounts', {
                url: '/linked-accounts',
                views: {
                    content: {
                        templateUrl: 'views/linked-accounts.html',
                        controller: 'LinkedAccountsCtrl as ctrl',
                    },
                },
            });
    }
})();
