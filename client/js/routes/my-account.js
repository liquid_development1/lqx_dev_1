;(function() {
    'use strict';

    angular
        .module('singboapp')
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                // Account Settings
                .state('MyAccount', {
                    url: '/MyAccount',
                    views: {
                        nav: {
                            templateUrl: 'views/navbar_1.html',
                        },
                        content: {
                            templateUrl: 'views/accountsettings.html',
                            controller: 'MyAccountCtrl as ctrl',
                        },
                    },
                })
                // Bank Transfer
                .state('MyAccount.BankTransfer', {
                    url: '/banktransfer',
                    views: {
                        content: {
                            templateUrl: 'views/banktransfer.html',
                        },
                    },
                })
                // General
                .state('MyAccount.General', {
                    url: '/general',
                    views: {
                        content: {
                            templateUrl: 'views/general.html',
                        },
                    },
                });
        }]);
})();
