;(function() {
    'use strict';

    angular
        .module('singboapp')
        .config(config)
        .run(handleUnauthorizedStateChanges);

    config.$inject = ['$stateProvider', 'UnverifiedSessionResolver'];

    function config($stateProvider, UnverifiedSessionResolver) {
        $stateProvider
            // Sign In
            .state('SignIn', {
                url: '/signIn',
                views: {
                    content: {
                        templateUrl: 'views/login_1.html',
                        controller: 'LoginCtrl as ctrl',
                    },
                },
            })
            // Register
            .state('SignUp', {
                url: '/signUp',
                views: {
                    content: {
                        templateUrl: 'views/register_1.html',
                        controller: 'RegisterCtrl as ctrl',
                    },
                },
            })
            // Reset
            .state('resetPassword', {
                url: '/resetPassword',
                views: {
                    nav: {
                        templateUrl: 'views/navbar.html',
                    },
                    content: {
                        templateUrl: 'views/resetpassword.html',
                        controller: 'ResetPasswordCtrl as ctrl',
                    },
                },
            })
            // Verify by SMS
            .state('Verify', {
                url: '/verify',
                views: {
                    content: {
                        templateUrl: 'views/verify.html',
                        controller: 'VerifyCtrl as ctrl',
                    },
                },
                resolve: {
                    _session: UnverifiedSessionResolver,
                }
            });
    }

    handleUnauthorizedStateChanges.$inject = ['$rootScope', '$state'];

    function handleUnauthorizedStateChanges($rootScope, $state) {
        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error, options) {

            if (error === 'Unauthorized') {
                $state.go('SignIn');
            }

            if (error === 'Session Unverified') {
                $state.go('Verify');
            }

            // TODO: return to Error page
        });
    }
})();
