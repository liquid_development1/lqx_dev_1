;(function() {
    'use strict';

    angular
        .module('singboapp')
        .config(config);

    config.$inject = ['$stateProvider', 'SessionResolver'];

    function config($stateProvider, SessionResolver) {
        $stateProvider
            .state('MacroOverview', {
                url: '/default-app',
                views: {
                    nav: {
                        templateUrl: 'views/navbar_1.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    content: {
                        templateUrl: 'views/macrooverview.html',
                    }
                },
                resolve: {
                    _session: SessionResolver,
                },
            })
            .state('MacroOverview.ExchangeRate', {
                url: '/exchangerate',
                views: {
                    content: {
                        templateUrl: 'views/exchangerate.html',
                        controller: 'ExchangeRateCtrl as ctrl',
                    },
                },
                resolve: {
                    _session: SessionResolver,
                },
            })
            
    }
})();
