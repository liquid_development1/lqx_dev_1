;(function () {
    'use strict';
    angular
        .module('singboapp')
        .config(['$urlRouterProvider', function($urlRouterProvider) {
            $urlRouterProvider.otherwise('/index-app');
        }])
        .config(config);

    config.$inject = ['$stateProvider', 'SessionResolver'];

    function config($stateProvider, SessionResolver) {
        $stateProvider
            // Home Page
            .state('indexApp', {
                url: '/index-app',
                views: {
                    content: {
                        templateUrl: 'views/index-app.html',
                    },
                },
            })
            // Portfolio
            .state('Portfolio', {
                url: '/portfolio',
                views: {
                    nav: {
                        templateUrl: 'views/navbar_1.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    content: {
                        templateUrl: 'views/portfolio.html',
                    },
                },
                resolve: {
                    _session: SessionResolver,
                },
            })
            .state('Portfolio.Investments', {
                url: '/investments',
                views: {
                    content: {
                        templateUrl: 'views/investments.html',
                        controller: 'BondoraInvestmentsCtrl as ctrl',
                    },
                },
            })
            // Loan Market
            .state('LoanMarket', {
                url: '/loanmarket',
                views: {
                    nav: {
                        templateUrl: 'views/navbar_1.html',
                        controller: 'NavCtrl as ctrl',
                    },
                    content: {
                        templateUrl: 'views/loanmarket.html',
                        controller: 'MarketCtrl as ctrl'
                    },
                    
                },
                resolve: {
                    _session: SessionResolver,
                },
            })
            .state('LoanMarket.BondoraAuctions', {
                url: '/bondora-auctions',
                views: {
                    content: {
                        templateUrl: 'views/bondora-auctions.html',
                        controller: 'BondoraAuctionsCtrl as ctrl',
                    },
                },
            })    
            // FAQs
            .state('Support', {
                url: '/custom-app',
                views: {
                    nav: {
                        templateUrl: 'views/navbar.html',
                    },
                    content: {
                        templateUrl: 'common/404-error.html',
                    },
                },
            });
    }

})();
