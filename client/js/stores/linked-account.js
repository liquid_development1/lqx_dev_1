;(function() {
    'use strict';

    angular
        .module('singboapp')
        .store('LinkedAccountStore', LinkedAccountStore);

    LinkedAccountStore.$inject = ['_', 'fluxActions'];

    function LinkedAccountStore(_, fluxActions) {
        return {
            initialize: function() {
                this.state = this.immutable({
                    refreshTokens: {},
                });
            },
            setRefreshTokens: function(linkedAccounts) {
                _.forEach(linkedAccounts, function(acc) {
                    this.state.set(['refreshTokens', acc.type], acc.refreshToken);
                }.bind(this));
            },
            updateRefreshToken: function(data) {
                this.state.set(['refreshTokens', data.type], data.refreshToken);
            },
            exports: {
                get refreshTokens() {
                    return this.state.get('refreshTokens');
                },
            },
            handlers: {
                [fluxActions.LINKED_ACCOUNT_FETCH_SUCCESS]: 'setRefreshTokens',
                [fluxActions.LINKED_ACCOUNT_UPDATE_REFRESH_TOKEN]: 'updateRefreshToken',
            },
        };
    }
})();
