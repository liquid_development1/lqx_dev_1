;(function() {
    'use strict';

    angular
        .module('singboapp')
        .store('BondoraStore', BondoraStore);

    BondoraStore.$inject = ['_', 'fluxActions'];

    function BondoraStore(_, fluxActions) {
        return {
            initialize: function() {
                this.state = this.immutable({
                    bids: [],
                    auctions: [],
                });
            },
            addBid: function(bids) {
                bids.forEach(function(bid) {
                    this.state.push('bids', bid);
                }.bind(this));
            },
            removeBid: function(bid) {
                var bids = this.state.get('bids');

                var next = _.filter(bids, function(b) {
                    return b.id !== bid.id;
                });

                this.setBids(next);
            },
            setBids: function(bids) {
                this.state.set('bids', bids);
            },
            setAuctions: function(auctions) {
                this.state.set('auctions', auctions);
            },
            setInvestments: function(investments) {
                this.state.set('investments', investments);
            },
            setBalance: function(balance) {
                this.state.set('balance', balance);
            },
            exports: {
                get bids() {
                    return this.state.get('bids');
                },
                get auctions() {
                    return this.state.get('auctions');
                },
                get investments() {
                    return this.state.get('investments');
                },
                get balance() {
                    return this.state.get('balance');
                },
            },
            handlers: {
                [fluxActions.BONDORA_BID_FETCH_SUCCESS]: 'setBids',
                [fluxActions.BONDORA_BID_ADD_SUCCESS]: 'addBid',
                [fluxActions.BONDORA_BID_REMOVE_SUCCESS]: 'removeBid',
                [fluxActions.BONDORA_AUCTION_FETCH_SUCCESS]: 'setAuctions',
                [fluxActions.BONDORA_INVESTMENT_FETCH_SUCCESS]: 'setInvestments',
                [fluxActions.BONDORA_BALANCE_FETCH_SUCCESS]: 'setBalance',
            },
        };
    }
})();
