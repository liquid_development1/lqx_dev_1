;(function() {
    'use strict';

    angular
        .module('singboapp')
        .store('MASStore', MASStore);

    MASStore.$inject = ['_', 'fluxActions'];

    function MASStore(_, fluxActions) {
        return {
            initialize: function() {
                this.state = this.immutable({
                    exchangeRates: [],
                });
            },
            setExchangeRates: function(exchangeRates) {
                this.state.set('exchangeRates', exchangeRates);
            },
            exports: {
                get exchangeRates() {
                    return this.state.get('exchangeRates');
                },
            },
            handlers: {
                [fluxActions.MAS_EXCHANGE_RATES_FETCH_SUCCESS]: 'setExchangeRates',
            },
        };
    }
})();
