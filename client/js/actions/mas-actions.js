;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('MASActions', MASActions);

    MASActions.$inject = ['$http', 'flux', 'fluxActions', 'EXCHANGE_RATE_LIST'];

    function MASActions($http, flux, fluxActions, EXCHANGE_RATE_LIST) {
        var actions = {
            fetchExchangeRates: function() {
                $http({
                    method: 'get',
                    url: '/api/exchange-rates/insight',
                    params: {
                        currencies: EXCHANGE_RATE_LIST.join(' '),
                    },
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.MAS_EXCHANGE_RATES_FETCH_SUCCESS, res.data);
                });
            },
        };

        return actions;
    }
})();
