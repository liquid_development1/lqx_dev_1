;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('BondoraActions', BondoraActions);

    BondoraActions.$inject = ['$http', 'flux', 'fluxActions'];

    function BondoraActions($http, flux, fluxActions) {
        var actions = {
            placeBids: function(bids) {
                if (!bids || bids.length === 0) {
                    return;
                }

                $http({
                    method: 'post',
                    url: '/api/bondora-bids',
                    data: bids,
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.BONDORA_BID_ADD_SUCCESS, res.data);
                });
            },
            fetchAuctions: function() {
                $http({
                    method: 'get',
                    url: '/api/bondora-auctions',
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.BONDORA_AUCTION_FETCH_SUCCESS, res.data);
                });
            },
            fetchInvestments: function() {
                $http({
                    method: 'get',
                    url: '/api/bondora-investments',
                })
                    .then(function(res) {
                        flux.dispatch(fluxActions.BONDORA_INVESTMENT_FETCH_SUCCESS, res.data);
                    });
            },
            fetchBids: function(userId) {
                $http({
                    method: 'get',
                    url: '/api/bondora-bids',
                    params: {
                        userId: userId,
                    },
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.BONDORA_BID_FETCH_SUCCESS, res.data);
                });
            },
            removeBid: function(bid) {
                $http({
                    method: 'delete',
                    url: '/api/bondora-bids/' + bid.id,
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.BONDORA_BID_REMOVE_SUCCESS, bid);
                });
            },
            checkout: function(userId) {
                $http({
                    method: 'post',
                    url: '/api/bondora-bids/checkout',
                    params: {
                        userId: userId,
                    },
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.BONDORA_BID_FETCH_SUCCESS, []);
                    flux.dispatch(fluxActions.BONDORA_BALANCE_FETCH_SUCCESS, res.data);
                });
            },
            fetchBalance: function(userId) {
                $http({
                    method: 'get',
                    url: '/api/bondora-balance',
                    params: {
                        userId: userId,
                    },
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.BONDORA_BALANCE_FETCH_SUCCESS, res.data);
                });
            },
        };

        return actions;
    };
})();
