;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('LinkedAccountActions', LinkedAccountActions);

    LinkedAccountActions.$inject = ['$http', 'flux', 'fluxActions'];

    function LinkedAccountActions($http, flux, fluxActions) {
        var actions = {
            fetch: function() {
                $http({
                    method: 'get',
                    url: '/api/linked-accounts',
                })
                .then(function(res) {
                    flux.dispatch(fluxActions.LINKED_ACCOUNT_FETCH_SUCCESS, res.data);
                });
            },
            updateRefreshToken: function(type, refreshToken) {
                flux.dispatch(fluxActions.LINKED_ACCOUNT_UPDATE_REFRESH_TOKEN, {
                    type: type,
                    refreshToken: refreshToken,
                });
            },
        };

        return actions;
    }

})();
