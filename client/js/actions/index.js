;(function() {
    'use strict';

    angular
        .module('singboapp')
        .constant('fluxActions', {
            // BONDORA
            BONDORA_BID_FETCH_SUCCESS: 'BONDORA_BID_FETCH_SUCCESS',
            BONDORA_BID_ADD_SUCCESS: 'BONDORA_BID_ADD_SUCCESS',
            BONDORA_BID_REMOVE_SUCCESS: 'BONDORA_BID_REMOVE_SUCCESS',
            BONDORA_AUCTION_FETCH_SUCCESS: 'BONDORA_AUCTION_FETCH_SUCCESS',
            BONDORA_INVESTMENT_FETCH_SUCCESS: 'BONDORA_INVESTMENT_FETCH_SUCCESS',
            BONDORA_BALANCE_FETCH_SUCCESS: 'BONDORA_BALANCE_FETCH_SUCCESS',
            // MAS
            MAS_EXCHANGE_RATES_FETCH_SUCCESS: 'MAS_EXCHANGE_RATES_FETCH_SUCCESS',
            // LINKED-ACCOUNTS
            LINKED_ACCOUNT_FETCH_SUCCESS: 'LINKED_ACCOUNT_FETCH_SUCCESS',
            LINKED_ACCOUNT_UPDATE_REFRESH_TOKEN: 'LINKED_ACCOUNT_UPDATE_REFRESH_TOKEN',
        })
        .config(['fluxProvider', function(fluxProvider) {
            fluxProvider.autoInjectStores(true);
        }]);
})();
