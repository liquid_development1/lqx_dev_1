;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('User', User);

    User.$inject = ['$resource'];

    function User($resource) {
        return $resource('/api/users', {
            id: '@id',
        });
    }
})();
