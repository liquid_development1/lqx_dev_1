;(function() {
    'use strict';

    angular
        .module('singboapp')
        .factory('Session', Session);

    Session.$inject = ['$resource'];

    function Session($resource) {
        return $resource('/api/sessions/:sid', {
            sid: '@sid',
        }, {
            verify: {
                method: 'PUT',
                params: {
                    sid: '@sid',
                },
            },
            requestVerifyToken: {
                method: 'POST',
                url: '/api/sessions/:sid/token',
                params: {
                    sid: '@sid',
                },
            },
        });
    }
})();
