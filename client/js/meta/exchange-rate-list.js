;(function() {
    'use strict';

    angular
        .module('singboapp')
        .constant('EXCHANGE_RATE_LIST', [
            'eurSgd',
            'gbpSgd',
            'usdSgd',
            'audSgd',
            'cadSgd',
            'cnySgd100',
        ]);
})();
