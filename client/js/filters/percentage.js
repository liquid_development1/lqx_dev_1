;(function() {
    'use strict';

    angular
        .module('singboapp')
        .filter('percentage', ['$filter', function($filter) {
            return function(input, fractionSize) {
                return (input > 0 ? '+' : '') + $filter('number')(input * 100, fractionSize) + '%';
            };
        }]);
})();
