;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$state', 'AuthService', 'Flash'];

    function LoginCtrl($state, AuthService, Flash) {
        var vm = this;

        vm.login = function() {
            return AuthService
                .login({
                    email: vm.user.email,
                    password: vm.user.password,
                })
                .then(function(session) {
                    vm.session = session;

                    if (session.verified) {
                        $state.go('MacroOverview');
                    } else {
                        $state.go('Verify');
                    }
                })
                .catch(function() {
                    // TODO: should wrapped by DialogService
                    Flash.create('danger', 'Ooops having issue logging in!', 0, {
                        class: 'custom-class',
                        id: 'custom-id',
                    }, true);
                });
        };
    }
})();
