'use strict';

;(function(){
    angular
        .module('singboapp')
        .controller('ResetPasswordCtrl', ResetPasswordCtrl)
        .controller('DefaultAppCtrl', DefaultAppCtrl)
        .controller('MarketCtrl', MarketCtrl);

    // RESET PASSWORD
    function ResetPasswordCtrl($http, $q, $sanitize){
        var vm = this;
        vm.emailAddress = '';

        vm.resetPassword = function () {
            console.info($sanitize(vm.emailAddress));

        };
    }

    ResetPasswordCtrl.$inject  = ['$http', '$q', '$sanitize'];

    // DEFAULT
    function DefaultAppCtrl() {
    }

    // MARKET
    function MarketCtrl() {
    }

})();
