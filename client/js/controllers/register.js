;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('RegisterCtrl', RegisterCtrl);

    RegisterCtrl.$inject = ['$sanitize', '$state', 'AuthService', 'Flash'];

    function RegisterCtrl($sanitize, $state, AuthService, Flash) {
        var vm = this;
        vm.email = '';
        vm.password = '';
        vm.confirmPassword = '';

        vm.register = function() {
            return AuthService
                .register({
                    name: $sanitize(vm.name),
                    email: $sanitize(vm.email),
                    password: $sanitize(vm.password),
                    countryCode: $sanitize(vm.countryCode),
                    phone: $sanitize(vm.phone),
                })
                .then(function() {
                    return AuthService
                        .login({
                            email: $sanitize(vm.email),
                            password: $sanitize(vm.password),
                        });
                })
                .then(function() {
                    $state.go('MacroOverview');
                });
        };
    }
})();
