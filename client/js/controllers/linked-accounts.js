;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('LinkedAccountsCtrl', LinkedAccountsCtrl);

    LinkedAccountsCtrl.$inject = ['$scope', '$window', 'LinkedAccountActions', 'LinkedAccountStore'];

    var OAUTH_URLS = {
        bondora: '/bondora/oauth',
    }

    function LinkedAccountsCtrl($scope, $window, LinkedAccountActions, LinkedAccountStore) {
        var vm = this;
        vm.dialogs = {
            bondora: null,
        };

        vm.oauth = function(site) {
            // prevent dialog open twice
            if (vm.dialogs[site]) {
                return;
            }

            var url = OAUTH_URLS[site];
            var dialog = vm.dialogs[site] = $window.open(url, 'oauth', 'menubar=no,status=no');

            dialog.addEventListener('message', function(message) {
                dialog.close();

                LinkedAccountActions.updateRefreshToken(site, message.data);
            }, false);

            dialog.addEventListener('unload', function() {
                vm.dialogs[site] = null;
            });
        };

        $scope.$listenTo(LinkedAccountStore, ['refreshTokens'], function() {
            console.log('LinkedAccountStore:refreshTokens changed', LinkedAccountStore.refreshTokens);

            vm.refreshTokens = LinkedAccountStore.refreshTokens;
        });

        LinkedAccountActions.fetch();
    }
})();
