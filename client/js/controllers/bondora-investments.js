;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('BondoraInvestmentsCtrl', BondoraInvestmentsCtrl);

    BondoraInvestmentsCtrl.$inject = ['$scope', '_', '_session', 'BondoraActions', 'BondoraStore'];

    function BondoraInvestmentsCtrl($scope, _, _session, BondoraActions, BondoraStore) {

        var vm = this;
        vm.expandStates = {};

        vm.expand = function(id) {
            vm.expandStates[id] = true;
        };

        vm.collapse = function(id) {
            vm.expandStates[id] = false;
        };

        $scope.$listenTo(BondoraStore, ['investments'], function() {
            console.log('BondoraStore:investments changed', BondoraStore.investments);
            vm.investments = BondoraStore.investments;
        });

        BondoraActions.fetchInvestments();

    }
})();
