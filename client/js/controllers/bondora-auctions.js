;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('BondoraAuctionsCtrl', BondoraAuctionsCtrl);

    BondoraAuctionsCtrl.$inject = ['$scope', '_', '_session', 'BondoraActions', 'BondoraStore'];

    function BondoraAuctionsCtrl($scope, _, _session, BondoraActions, BondoraStore) {
        var vm = this;
        vm.expandStates = {};
        vm.selections = {};
        vm.bidValues = {};
        vm.allSelected = false;

        vm.expand = function(id) {
            vm.expandStates[id] = true;
        };

        vm.collapse = function(id) {
            vm.expandStates[id] = false;
        }

        vm.toggleSelectAll = function() {
            _.forEach(vm.auctions, function(auction) {
                vm.selections[auction.auctionId] = vm.allSelected;
            });
        };

        vm.auctionSelected = function(auction) {
            // TODO should count for avoiding loop over vm.auctions, but implement later
            var statuses = _.countBy(vm.selections);

            vm.allSelected = statuses.true === vm.auctions.length;
        };

        vm.removeBid = function(bid) {
            BondoraActions.removeBid(bid);
        };

        vm.checkout = function() {
            BondoraActions.checkout(_session.userId);
        };

        vm.placeBids = function() {
            var bids = [];

            _.forEach(vm.bidValues, function(bidValue, auctionId) {
                var shouldAdd = vm.selections[auctionId] && !isNaN(bidValue) && bidValue;

                if (shouldAdd) {
                    // vm.selections[auctionId] = false;
                    // vm.bidValues[auctionId] = null;

                    bids.push({
                        userId: _session.userId,
                        auctionId: auctionId,
                        bidValue: parseFloat(bidValue),
                    });
                }
            });

            // console.log(bids);

            BondoraActions.placeBids(bids);
        };

        $scope.$listenTo(BondoraStore, ['bids'], function() {
            console.log('BondoraStore:bids changed', BondoraStore.bids);

            vm.bids = BondoraStore.bids;

            vm.totalBid = _.reduce(vm.bids, function(total, bid) {
                return total + bid.bidValue;
            }, 0);
        });

        $scope.$listenTo(BondoraStore, ['auctions'], function() {
            console.log('BondoraStore:auctions changed', BondoraStore.auctions);
            vm.auctions = BondoraStore.auctions;
        });

        $scope.$listenTo(BondoraStore, ['balance'], function() {
            console.log('BondoraStore:balance changed', BondoraStore.balance);
            vm.balance = BondoraStore.balance;
        });

        BondoraActions.fetchAuctions();
        BondoraActions.fetchBids(_session.userId);
        BondoraActions.fetchBalance(_session.userId);
    }
})();
