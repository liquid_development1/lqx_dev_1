;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('ExchangeRateCtrl', ExchangeRateCtrl);

    function ExchangeRateCtrl($scope, MASActions, MASStore) {
        var vm = this;

        $scope.$listenTo(MASStore, ['exchangeRates'], function() {
            console.log('MASStore:exchangeRates changed', MASStore.exchangeRates);

            vm.exchangeRates = MASStore.exchangeRates;
        });

        MASActions.fetchExchangeRates();
    }

    ExchangeRateCtrl.$inject = ['$scope', 'MASActions', 'MASStore'];
})();
