;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('NavCtrl', NavCtrl);

    NavCtrl.$inject = ['_session'];

    function NavCtrl(_session) {
        var vm = this;

        vm.session = _session;
    }
})();
