;(function() {
    'use strict';

    angular
        .module('singboapp')
        .controller('VerifyCtrl', VerifyCtrl);

    VerifyCtrl.$inject = ['_session', '$state'];

    function VerifyCtrl(_session, $state) {
        var vm = this;
        vm.session = _session;
        vm.verify = function() {
            _session
                .$verify()
                .then(function() {
                    $state.go('MacroOverview');
                });
        };

        if (_session.verified) {
            $state.go('MacroOverview');
        } else {
            _session.$requestVerifyToken();
        }
    }
})();
