;(function() {
    'use strict';

    angular
        .module('singboapp')
        .directive('sparkline', sparkline);

    sparkline.$inject = ['_', 'd3'];

    function sparkline(_, d3) {
        return {
            restrict: 'E',
            scope: {
                history: '=',
            },
            link: function(scope, element, attr) {
                var width = 300; // TODO remove harded-code value
                var height = element.parent().height();

                var x = d3.scaleLinear().range([2, width - 4]);
                var y = d3.scaleLinear().range([height - 4, 2]);

                var parseDate = d3.timeParse('%Y-%m-%d');
                var line = d3.line()
                    .curve(d3.curveBasis)
                    .x(function(d) {
                        return x(d.date);
                    })
                    .y(function(d) {
                        return y(d.value);
                    });

                var data = _.map(scope.history, function(d) {
                    return {
                        date: parseDate(d.time).valueOf(),
                        value: d.value,
                    };
                });

                x.domain(d3.extent(data, function(d) {
                    return d.date;
                }));
                y.domain(d3.extent(data, function(d) {
                    return d.value;
                }));

                var svg = d3.select(element[0])
                    .append('svg')
                    .attr('width', width)
                    .attr('height', height)
                    .append('g');

                svg.append('path')
                    .datum(data)
                    .attr('d', line)
                    .attr('class', 'sparkline');
            },
        };
    }
})();
