;(function() {
    'use strict';

    angular
        .module('singboapp')
        .directive('profileIcon', profileIcon);

    profileIcon.$inject = ['$window'];

    function profileIcon($window) {
        return {
            restrict: 'E',
            templateUrl: '/js/ui-components/profile-icon.html',
            scope: {
                session: '=',
            },
            controller: ['$scope', '$state', 'AuthService', function($scope, $state, AuthService) {
                $scope.menuEnabled = false;

                $scope.toggleMenu = function() {
                    $scope.menuEnabled = !$scope.menuEnabled;
                };

                $scope.logout = function() {
                    AuthService.logout().then(function() {
                        $state.go('indexApp');
                    });
                };
            }],
        };
    }
})();
