'use strict';

const schedule = require('node-schedule');

const config = require('../shared/config');
const Client = require('../shared/mq').Client;

const mqClient = new Client(config.mq.host);

require('./jobs').forEach(scheduleJob);

function scheduleJob(job) {
    if (!job.enable) {
        return;
    }

    if (job.pattern) {
        schedule.scheduleJob(job.pattern, execute.bind(null, job));
    }

    if (job.immediate) {
        execute(job);
    }
}

function execute(job) {
    var startTime = new Date();

    console.log('On schedule:', startTime.toISOString(), job);

    mqClient
        .getConnection()
        .then((conn) => {
            if (!conn) {
                console.log('Could not connect to MQ');
                return;
            }

            return publishMessage(startTime, conn, job);
        })
        .then(() => {
            console.log('Send message done');
        })
        .catch((e) => {
            console.error('Send message failed', e);
        });
}

function publishMessage(startTime, conn, job) {
    return conn.createChannel()
        .then((ch) => {
            return [
                ch,
                ch.assertExchange(config.mq.schedulerExchange, 'direct', {
                    autoDelete: true,
                    durable: true,
                })
            ];
        })
        .spread((ch, assertEx) => {
            var content = new Buffer(job.data ? JSON.stringify(job.data) : '');

            return ch.publish(assertEx.exchange, job.routeKey, content, {
                contentType: 'application/json',
                contentEncoding: 'utf-8',
                timestamp: startTime.valueOf(),
            });
        });
}
