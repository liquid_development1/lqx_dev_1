'use strict';

module.exports = [{
    // enable/disable this job definition
    enable: false,
    // cron-pattern,
    pattern: '0 0 * * *', // every day,
    // execute this job immediately right after the process run
    immediate: true,
    // routeKey for RabbitMQ
    routeKey: 'full-update-exchange-rates',
    // data, if available
    data: null
}, {
    enable: false,
    pattern: '*/5 * * * *',
    routeKey: 'update-exchange-rates',
    data: {}
}, {
    enable: false,
    pattern: '*/30 * * * * *', // every 30 seconds
    routeKey: 'update-bondora-auctions'
}, {
    enable: false,
    pattern: '*/30 * * * * *',
    routeKey: 'update-bondora-balance'
}, {
    enable: false,
    pattern: '*/30 * * * * *',
    routeKey: 'update-bondora-investments'
}, {
    enable: false,
    pattern: '0 0 * * *',
    immediate: false,
    routeKey: 'update-bondora-loandataset',
}, {
    enable: true,
    pattern: '0 0 * * *',
    immediate: true,
    routeKey: 'update-bondora-publicdataset'
}];
