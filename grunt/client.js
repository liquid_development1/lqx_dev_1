'use strict';

exports.config = {
    injector: {
        options: {
            template: 'client/index.tpl.html',
        },
        static: {
            files: {
                'client/index.html': [
                    'bower.json',
                    'bower_components/uikit/dist/css/uikit.css',
                    'bower_components/uikit/dist/js/uikit.js',
                    'client/css/**/*.css',
                    'client/js/index.js',
                    'client/js/**/*.js',
                ],
            },
        },
    },
    watch: {
        client: {
            files: [
                'client/**/*.{html,js,css}',
                'bower.json',
            ],
            tasks: ['injector:static'],
        },
    },
};

exports.register = function(grunt) {
    grunt.registerTask('client', [
        'injector:static',
        'watch:client',
    ]);
};
