'use strict';

exports.config = {
    develop: {
        server: {
            file: 'index.js',
        },
    },
    watch: {
        server: {
            files: [
                'index.js',
                'shared/**/*.js',
                'server/**/*.js',
                '!server/scheduler/**/*.js',
            ],
            tasks: ['develop:server'],
        },
    },
};

exports.register = function(grunt) {
    grunt.registerTask('server', [
        'develop:server',
        'watch:server',
    ]);
};
