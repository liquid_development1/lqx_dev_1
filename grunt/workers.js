'use strict';

exports.config = {
    concurrent: {
        workers: [
            [
                'develop:update-exchange-rates',
                'watch:update-exchange-rates',
            ],
            [
                'develop:full-update-exchange-rates',
                'watch:full-update-exchange-rates',
            ],
            [
                'develop:update-bondora-auctions',
                'watch:update-bondora-auctions',
            ],
            [
                'develop:update-bondora-balance',
                'watch:update-bondora-balance',
            ],
            [
                'develop:update-bondora-investments',
                'watch:update-bondora-investments',
            ],
            [
                'develop:update-bondora-loandataset',
                'watch:update-bondora-loandataset',
            ],
            [
                'develop:update-bondora-publicdataset',
                'watch:update-bondora-publicdataset',
            ]
        ]
    },
    develop: {
        'update-exchange-rates': {
            file: 'workers/update-exchange-rates/index.js',
        },
        'full-update-exchange-rates': {
            file: 'workers/full-update-exchange-rates/index.js',
        },
        'update-bondora-auctions': {
            file: 'workers/update-bondora-auctions/index.js',
        },
        'update-bondora-balance': {
            file: 'workers/update-bondora-balance/index.js',
        },
        'update-bondora-investments': {
            file: 'workers/update-bondora-investments/index.js',
        },
        'update-bondora-loandataset': {
            file: 'workers/update-bondora-loandataset/index.js',
        },
        'update-bondora-publicdataset': {
            file: 'workers/update-bondora-publicdataset/index.js',
        }
    },
    watch: {
        'update-exchange-rates': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-exchange-rates/**/*.js',
            ],
            tasks: ['develop:update-exchange-rates'],
        },
        'full-update-exchange-rates': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-exchange-rates/**/*.js',
                'workers/full-update-exchange-rates/**/*.js',
            ],
            tasks: ['develop:full-update-exchange-rates'],
        },
        'update-bondora-auctions': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-bondora-auctions/**/*.js',
            ],
            tasks: ['develop:update-bondora-auctions'],
        },
        'update-bondora-balance': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-bondora-balance/**/*.js',
            ],
            tasks: ['develop:update-bondora-balance'],
        },
        'update-bondora-investments': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-bondora-investments/**/*.js',
            ],
            tasks: ['develop:update-bondora-investments'],
        },
        'update-bondora-loandataset': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-bondora-loandataset/**/*.js',
            ],
            tasks: ['develop:update-bondora-loandataset'],
        },
        'update-bondora-publicdataset': {
            files: [
                'shared/**/*.js',
                'workers/mq-helper.js',
                'workers/update-bondora-publicdataset/**/*.js',
            ],
            tasks: ['develop:update-bondora-publicdataset'],
        }
    }
};

exports.register = function(grunt) {
    grunt.registerTask('workers', [
        'concurrent:workers',
    ]);
};
