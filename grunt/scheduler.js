'use strict';

exports.config = {
    develop: {
        scheduler: {
            file: 'scheduler/index.js',
        },
    },
    watch: {
        scheduler: {
            files: [
                'shared/mq/**/*.js',
                'scheduler/**/*.js',
            ],
            tasks: ['develop:scheduler'],
        },
    },
};

exports.register = function(grunt) {
    grunt.registerTask('scheduler', [
        'develop:scheduler',
        'watch:scheduler',
    ]);
};
