'use strict';

// use .spread
global.Promise = require('bluebird');
const MqClient = require('../shared/mq').Client;

exports.subscribe = function(sub, cb) {
    const client = new MqClient(sub.host);

    client
    .getConnection()
    .then((conn) => {
        return conn.createChannel();
    })
    .then((ch) => {
        return [
            ch,
            ch.assertExchange(sub.exchangeName, 'direct', {
                autoDelete: true,
                durable: true,
            }),
            ch.assertQueue(sub.queueName, {
                durable: true,
                autoDelete: true,
            }),
            ch.bindQueue(sub.queueName, sub.exchangeName, sub.routeKey),
        ];
    })
    .spread((ch, assertEx, assertQ) => {
        ch.consume(assertQ.queue, (msg) => {
            if (typeof cb === 'function') {
                cb(msg);
            }
        });
    })
    .catch((e) => console.error(e));
};

exports.deserialize = function(msg) {
    var content = msg.content.toString();

    if (msg.properties.contentType === 'application/json') {
        return JSON.parse(content);
    }

    return msg.content.toString();
};
