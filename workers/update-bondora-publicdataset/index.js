'use strict';

const mq = require('../mq-helper');

// load databases
const config = require('../../shared/config');
const mongodb = require('../../shared/mongodb');
const sqldb = require('../../shared/sqldb');
const bondoraHelper = require('../../shared/bondora-api/helper');

const getPublicDataSet = require('./get-publicdataset');
const savePublicDataSet = require('./save-publicdataset');

mq.subscribe({
    host: config.mq.host,
    exchangeName: config.mq.schedulerExchange,
    routeKey: 'update-bondora-publicdataset',
    queueName: 'lqx.update-bondora-publicdataset.local',
}, (msg) => execute(1));

function execute(page) {
    return bondoraHelper
        .getAccessToken()
        .then(getPublicDataSet.bind(null, page))
        .then((data) => {
            return Promise.all([
                {
                    continue: data.Count + (data.PageSize * (data.PageNr - 1)) < data.TotalCount,
                    nextPage: data.PageNr + 1,
                },
                savePublicDataSet(data),
            ]);
        })
        .spread((decision) => {
            if (decision.continue) {
                return execute(decision.nextPage);
            }
        });
}
