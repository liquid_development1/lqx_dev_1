'use strict';

const models = require('../../shared/sqldb');

module.exports = function(data) {
    var auctions = data.Payload.map(fromRaw);
    var ids = auctions.map((auction) => auction.loanId);

    return models.bondoraAuctions.destroy(ids.length ? {
        // delete all expired records
        where: {
            loanId: {
                $notIn: ids,
            },
        },
    } : {
        // delete all records
        truncate: true,
    }).then(() => models.bondoraAuctions.bulkCreate(auctions, {
        updateOnDuplicate: true,
    }));
};

function fromRaw(auction) {
    return {
        loanId: auction.LoanId,
        auctionId: auction.AuctionId,
        loanNumber: auction.LoanNumber,
        userName: auction.UserName,
        newCreditNumber: auction.NewCreditCustomer,
        loanApplicationStartedDate: auction.LoanApplicationStartedDate,
        plannedCloseDate: auction.PlannedCloseDate,
        applicationSignedHour: auction.ApplicationSignedHour,
        applicationSignedWeekday: auction.ApplicationSignedWeekday,
        verificationType: auction.VerificationType,
        languageCode: auction.LanguageCode,
        age: auction.Age,
        dateOfBirth: auction.DateOfBirth,
        gender: auction.Gender,
        country: auction.Country,
        creditScoreEsMicroL: auction.CreditScoreEsMicroL,
        creditScoreEsEquifaxRisk: auction.CreditScoreEsEquifaxRisk,
        creditScoreFiAsiakasTietoRiskGrade: auction.CreditScoreFiAsiakasTietoRiskGrade,
        creditScoreEeMini: auction.CreditScoreEeMini,
        appliedAmount: auction.AppliedAmount,
        interest: auction.Interest,
        loanDuration: auction.LoanDuration,
        county: auction.County,
        city: auction.City,
        useOfLoan: auction.UseOfLoan,
        education: auction.Education,
        maritalStatus: auction.MaritalStatus,
        nrOfDependants: auction.NrOfDependants,
        employmentStatus: auction.EmploymentStatus,
        employmentDurationCurrentEmployer: auction.EmploymentDurationCurrentEmployer,
        employmentPosition: auction.EmploymentPosition,
        workExperience: auction.WorkExperience,
        occupationArea: auction.OccupationArea,
        homeOwnershipType: auction.HomeOwnershipType,
        incomeFromPrincipalEmployer: auction.IncomeFromPrincipalEmployer,
        incomeFromPension: auction.IncomeFromPension,
        incomeFromFamilyAllowance: auction.IncomeFromFamilyAllowance,
        incomeFromSocialWelfare: auction.IncomeFromSocialWelfare,
        incomeFromLeavePay: auction.IncomeFromLeavePay,
        incomeFromChildSupport: auction.IncomeFromChildSupport,
        incomeOther: auction.IncomeOther,
        incomeTotal: auction.IncomeTotal,
        freeCash: auction.FreeCash,
        debtToIncome: auction.DebtToIncome,
        monthlyPayment: auction.MonthlyPayment,
        monthlyPaymentDay: auction.MonthlyPaymentDay,
        modelVersion: auction.ModelVersion,
        expectedLoss: auction.ExpectedLoss,
        rating: auction.Rating,
        lossGivenDefault: auction.LossGivenDefault,
        probabilityOfDefault: auction.ProbabilityOfDefault,
        expectedReturnAlpha: auction.ExpectedReturnAlpha,
        liabilitiesTotal: auction.LiabilitiesTotal,
        listedOnUTC: auction.ListedOnUTC,
        actualCloseDate: auction.ActualCloseDate,
        winningBidsAmount: auction.WinningBidsAmount,
        remainingAmount: auction.RemainingAmount,
        userBids: auction.UserBids,
        userBidAmount: auction.UserBidAmount,
        fullfilled: auction.Fullfilled,
        creditScore: auction.CreditScore,
        scoringDate: auction.ScoringDate,
        eadRate: auction.EADRate,
        maturityFactor: auction.MaturityFactor,
        interestRateAlpha: auction.InterestRateAlpha,
    };
};
