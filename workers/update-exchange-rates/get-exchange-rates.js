'use strict';

const querystring = require('querystring');
const request = require('request');

const MAS_API_URL = 'https://eservices.mas.gov.sg/api/action/datastore/search.json';
const RESOURCE_ID_PARAM_NAME = 'resource_id';
const RESOURCE_ID_PARAM_VALUE = '95932927-c8bc-4e7a-b484-68a66a24edfe';
const SORT_PARAM_NAME = 'sort';
const SORT_PARAM_VALUE = 'end_of_day desc';
const LIMIT_PARAM_VALUE = 'limit';
const OFFSET_PARAM_VALUE = 'offset';

module.exports = function(offset, limit) {
    return new Promise((resolve, reject) => {
        request({
            url: getURL(offset, limit),
            json: true,
        }, (error, response, body) => {
            if (error) {
                return reject(error);
            }

            if (!response || !body || response.statusCode !== 200) {
                return reject(new Error('Unexpected response from mas.gov.sg'));
            }

            resolve(body);
        });
    });
};

function getURL(offset, limit) {
    var params = {};
    params[RESOURCE_ID_PARAM_NAME] = RESOURCE_ID_PARAM_VALUE;
    params[SORT_PARAM_NAME] = SORT_PARAM_VALUE;

    if (offset) {
        params[OFFSET_PARAM_VALUE] = offset;
    }

    if (limit) {
        params[LIMIT_PARAM_VALUE] = limit;
    }

    return MAS_API_URL + '?' + querystring.stringify(params);
}
