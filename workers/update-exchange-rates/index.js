'use strict';

const mq = require('../mq-helper');
const config = require('../../shared/config');

const getExchangeRates = require('./get-exchange-rates');
const saveExchangeRates = require('./save-exchange-rates');

const LIMIT = 10;

mq.subscribe({
    host: config.mq.host,
    exchangeName: config.mq.schedulerExchange,
    routeKey: 'update-exchange-rates',
    queueName: 'lqx.update-exchange-rate.local',
}, (msg) => getExchangeRates(0, LIMIT).then(saveExchangeRates));
