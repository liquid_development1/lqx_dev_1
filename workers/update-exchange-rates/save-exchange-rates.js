'use strict';

const models = require('../../shared/sqldb');

module.exports = function(data) {
    return models.exchangeRateMas.bulkCreate(data.result.records.map(fromRaw), {
        updateOnDuplicate: true,
    });
};

function fromRaw(record) {
    return {
        endOfDay: record.end_of_day,
        preliminary: record.preliminary,
        eurSgd: record.eur_sgd,
        gbpSgd: record.gbp_sgd,
        usdSgd: record.usd_sgd,
        audSgd: record.aud_sgd,
        cadSgd: record.cad_sgd,
        cnySgd100: record.cny_sgd_100,
        hkdSgd100: record.hkd_sgd_100,
        inrSgd100: record.inr_sgd_100,
        idrSgd100: record.idr_sgd_100,
        jpySgd100: record.jpy_sgd_100,
        krwSgd100: record.krw_sgd_100,
        myrSgd100: record.myr_sgd_100,
        twdSgd100: record.twd_sgd_100,
        nzdSgd: record.nzd_sgd,
        phpSgd100: record.php_sgd_100,
        qarSgd100: record.qar_sgd_100,
        sarSgd100: record.sar_sgd_100,
        chfSgd: record.chf_sgd,
        thbSgd100: record.thb_sgd_100,
        aedSgd100: record.aed_sgd_100,
        vndSgd100: record.vnd_sgd_100,
        feedsFlatstoreEntryId: record.feeds_flatstore_entry_id,
        timestamp: record.timestamp,
        feedsEntityId: record.feeds_entity_id,
    };
};
