'use strict';

const mq = require('../mq-helper');
const config = require('../../shared/config');

const getExchangeRates = require('../update-exchange-rates/get-exchange-rates');
const saveExchangeRates = require('../update-exchange-rates/save-exchange-rates');

const LIMIT = 10;

mq.subscribe({
    host: config.mq.host,
    exchangeName: config.mq.schedulerExchange,
    routeKey: 'full-update-exchange-rates',
    queueName: 'lqx.full-update-exchange-rates.local',
}, (msg) => execute(0));

function execute(offset) {
    return getExchangeRates(offset)
        .then((data) => {
            console.log(data);
            return Promise.all([
                {
                    continue: data.result.limit + offset < Number(data.result.total),
                    limit: data.result.limit,
                },
                saveExchangeRates(data),
            ]);
        })
        .spread((decision) => {
            if (decision.continue) {
                return execute(offset + decision.limit);
            }
        });
}
