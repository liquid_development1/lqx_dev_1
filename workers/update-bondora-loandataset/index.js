'use strict';

const mq = require('../mq-helper');

// load databases
const config = require('../../shared/config');
const mongodb = require('../../shared/mongodb');
const sqldb = require('../../shared/sqldb');
const bondoraHelper = require('../../shared/bondora-api/helper');

const getLoanDataSet = require('./get-loandataset');
const saveLoanDataSet = require('./save-loandataset');

mq.subscribe({
    host: config.mq.host,
    exchangeName: config.mq.schedulerExchange,
    routeKey: 'update-bondora-loandataset',
    queueName: 'lqx.update-bondora-loandataset.local',
}, (msg) => execute(1));

function execute(page) {
    return bondoraHelper
        .getAccessToken()
        .then(getLoanDataSet.bind(null, page))
        .then((data) => {
            return Promise.all([
                {
                    continue: data.Count + (data.PageSize * (data.PageNr - 1)) < data.TotalCount,
                    nextPage: data.PageNr + 1,
                },
                saveLoanDataSet(data),
            ]);
        })
        .spread((decision) => {
            if (decision.continue) {
                return execute(decision.nextPage);
            }
        });
}
