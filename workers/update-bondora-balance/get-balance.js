'use strict';

const request = require('request');

module.exports = function(accessToken) {
    return new Promise((resolve, reject) => {
        request({
            url: 'https://api.bondora.com/api/v1/account/balance',
            method: 'get',
            auth: {
                bearer: accessToken,
            },
            json: true,
        }, (error, response, body) => {
            if (error) {
                return reject(error);
            }

            if (!body || !body.Success) {
                return reject(body);
            }

            resolve(body);
        })
    });
}
