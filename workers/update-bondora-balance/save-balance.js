'use strict';

const models = require('../../shared/sqldb');

module.exports = function(data) {
    return models.bondoraBalance.bulkCreate([{
        userId: 0,
        balance: data.Payload.TotalAvailable,
    }], {
        updateOnDuplicate: true,
    });
};
