'use strict';

const mq = require('../mq-helper');

// load databases
const config = require('../../shared/config');
const mongodb = require('../../shared/mongodb');
const sqldb = require('../../shared/sqldb');
const bondoraHelper = require('../../shared/bondora-api/helper');

const getBalance = require('./get-balance');
const saveBalance = require('./save-balance');

mq.subscribe({
    host: config.mq.host,
    exchangeName: config.mq.schedulerExchange,
    routeKey: 'update-bondora-balance',
    queueName: 'lqx.update-bondora-balance.local',
}, execute);

function execute() {
    return bondoraHelper
        .getAccessToken()
        .then(getBalance)
        .then(saveBalance);
}
