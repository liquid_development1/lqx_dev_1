'use strict';

const models = require('../../shared/sqldb');

module.exports = function(data) {
    var investments = data.Payload.map(fromRaw);
    var ids = investments.map((investment) => investment.loanPartId);

    return models.bondoraInvestments.destroy(ids.length ? {
        // delete all expired records
        where: {
            loanPartId: {
                $notIn: ids,
            },
        },
    } : {
        // delete all records
        truncate: true,
    }).then(() => models.bondoraInvestments.bulkCreate(investments, {
        updateOnDuplicate: true,
    }));
};


function fromRaw(investment) {
    return {
        loanPartId: investment.LoanPartId,
        amount: investment.Amount,
        auctionId: investment.AuctionId,
        auctionName: investment.AuctionName,
        auctionNumber: investment.AuctionNumber,
        auctionBidNumber: investment.AuctionBidNumber,
        auctionBidType: investment.AuctionBidType,
        country: investment.Country,
        creditScore: investment.CreditScore,
        creditScoreEsMicroL: investment.CreditScoreEsMicroL,
        creditScoreEsEquifaxRisk: investment.CreditScoreEsEquifaxRisk,
        creditScoreFiAsiakasTietoRiskGrade: investment.CreditScoreFiAsiakasTietoRiskGrade,
        creditScoreEeMini: investment.CreditScoreEeMini,
        rating: investment.Rating,
        interest: investment.Interest,
        useOfLoan: investment.UseOfLoan,
        incomeVerificationStatus: investment.IncomeVerificationStatus,
        loanId: investment.LoanId,
        loanStatusCode: investment.LoanStatusCode,
        loanStatusActiveFrom: investment.LoanStatusActiveFrom,
        userName: investment.UserName,
        gender: investment.Gender,
        dateOfBirth: investment.DateOfBirth,
        signedDate: investment.SignedDate,
        reScheduledOn: investment.ReScheduledOn,
        debtOccurredOn: investment.DebtOccurredOn,
        debtOccurredOnForSecondary: investment.DebtOccurredOnForSecondary,
        loanDuration: investment.LoanDuration,
        nextPaymentNr: investment.NextPaymentNr,
        nextPaymentDate: investment.NextPaymentDate,
        nextPaymentSum: investment.NextPaymentSum,
        nrOfScheduledPayments: investment.NrOfScheduledPayments,
        lastPaymentDate: investment.LastPaymentDate,
        principalRepaid: investment.PrincipalRepaid,
        interestRepaid: investment.InterestRepaid,
        lateAmountPaid: investment.LateAmountPaid,
        principalRemaining: investment.PrincipalRemaining,
        principalLateAmount: investment.PrincipalLateAmount,
        interestLateAmount: investment.InterestLateAmount,
        penaltyLateAmount: investment.PenaltyLateAmount,
        lateAmountTotal: investment.LateAmountTotal,
        principalWriteOffAmount: investment.PrincipalWriteOffAmount,
        interestWriteOffAmount: investment.InterestWriteOffAmount,
        penaltyWriteOffAmount: investment.PenaltyWriteOffAmount,
        debtServicingCostMainAmount: investment.DebtServicingCostMainAmount,
        debtServicingCostInterestAmount: investment.DebtServicingCostInterestAmount,
        debtServicingCostPenaltyAmount: investment.DebtServicingCostPenaltyAmount,
        purchaseDate: investment.PurchaseDate,
        soldDate: investment.SoldDate,
        purchasePrice: investment.PurchasePrice,
        salePrice: investment.SalePrice,
        listedInSecondMarketOn: investment.ListedInSecondMarketOn,
        latestDebtManagementStage: investment.LatestDebtManagementStage,
        latestDebtManagementStageType: investment.LatestDebtManagementStageType,
        latestDebtManagementDate: investment.LatestDebtManagementDate,
        noteLoanTransfersMainAmount: investment.NoteLoanTransfersMainAmount,
        noteLoanTransfersInterestAmount: investment.NoteLoanTransfersInterestAmount,
        noteLoanLateChargesPaid: investment.NoteLoanLateChargesPaid,
        noteLoanTransfersEarningsAmount: investment.NoteLoanTransfersEarningsAmount,
        noteLoanTransfersTotalRepaimentsAmount: investment.NoteLoanTransfersTotalRepaimentsAmount,

    };
};

