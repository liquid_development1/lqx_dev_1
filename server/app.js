'use strict';

const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const ect = require('ect');
const express = require('express');
const flash = require('connect-flash');
const passport = require('passport');
const path = require('path');
const session = require('express-session');

const importDir = require('./import-dir');

const app = express();

// load shared components & init db
const config = require('../shared/config');
const mongodb = require('../shared/mongodb');
const sqldb = require('../shared/sqldb');

// set shared components as application properties
app.set('config', config);
app.set('mongodb', mongodb);
app.set('sqldb', sqldb);

// public static files
app.use(express.static(path.resolve(__dirname, '../client')));
app.use('/client', express.static(path.resolve(__dirname, '../client')));
app.use('/bower_components', express.static(path.resolve(__dirname, '../bower_components')));

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

//Initialize passport
app.use(passport.initialize());

// do not use session for stateless
// app.use(passport.session());

// connect flash
app.use(flash());

// config server-side renderer
var viewDir = path.resolve(__dirname, './views');
var ectRenderer = ect({
    watch: true,
    root: viewDir,
    ext : '.ect'
});

app.set('view engine', 'ect');
app.set('views', viewDir);
app.engine('ect', ectRenderer.render);

// load our routes and pass in our app and fully configured passport
require('./auth.js')(app, passport);

// walk `api` folder for API routing
importDir(path.resolve(__dirname, './api', '*.js'), app);

// walk `routes` folder for webpage routing
importDir(path.resolve(__dirname, './routes', '*.js'), app);

// load our routes and pass in our app and fully configured passport
require('./routes.js')(app);

module.exports = app;
