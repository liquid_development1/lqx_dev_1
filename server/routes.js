'use strict';

const bcrypt   = require('bcrypt-nodejs');
const passport = require('passport');

module.exports = function(app) {
    const models = app.get('sqldb');

    app.post('/register', function(req, res) {
        if(!req.body.password === req.body.confirmpassword) {
            return res.status(500).json({
                err: err
            });
        }

        var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

        models.users.findOrCreate({
            where: {
                email: req.body.username,
            },
            defaults: {
                email: req.body.username,
                encryptedPassword: hashpassword,
            }
        }).spread(function (user, created) {
            if (created) {
                user.encryptedPassword = '';
                res.status(200);
                returnResults(user,res);
            } else {
                user.encryptedPassword = '';
                res.status(500);
                returnResults(user,res);
            }
        }).error(function (error) {
            res.status(500);
            returnResults(error,res);
        });
    });

    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('../');
    });

    app.post('/login', passport.authenticate('local', {
        successRedirect: '/home',
        failureRedirect: '/',
        failureFlash : true
    }));

    app.get('/status/user', function (req, res) {
        var status = '';
        if(req.user) {
            status = req.user.email;
        }
        console.info('status of the user ??? ' + status);
        res.send(status).end();
    });

    app.get('/logout', function(req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.send(req.user).end();
    });

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/');
    }

    function returnResults(results, res) {
        res.send(results);
    }
};
