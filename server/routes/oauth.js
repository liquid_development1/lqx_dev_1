'use strict';

const querystring = require('querystring');
const request = require('request');
const LinkedAccount = require('mongoose').model('LinkedAccount');

module.exports = function(app) {
    const config = app.get('config');

    app.route('/bondora/oauth')
        .get((req, res, next) => {
            var qs = querystring.stringify({
                response_type: 'code',
                scope: 'BidsEdit BidsRead Investments SmBuy SmSell',
                client_id: config.oauth.bondora.clientId,
                redirect_uri: config.oauth.bondora.callbackUrl,
            });

            res.redirect('https://www.bondora.com/oauth/authorize?' + qs);
        });

    app.route('/bondora/oauth-callback')
        .get((req, res, next) => {
            var code = req.query.code;

            request({
                method: 'post',
                url: 'https://api.bondora.com/oauth/access_token',
                form: {
                    code: code,
                    grant_type: 'authorization_code',
                    client_id: config.oauth.bondora.clientId,
                    client_secret: config.oauth.bondora.clientSecret,
                    redirect_uri: config.oauth.bondora.callbackUrl,
                },
                json: true,
            }, function(err, response, body) {
                if (err) {
                    return res.status(500).json(err);
                }

                // store refresh token in db
                LinkedAccount
                    .findOneAndUpdate({
                        type: 'bondora',
                    }, {
                        type: 'bondora',
                        refreshToken: body.refresh_token,
                    }, {
                        upsert: true,
                    })
                    .then(() => res.render('oauth-callback', {
                        refreshToken: body.refresh_token,
                        origin: config.origin,
                    }))
                    .catch((err) => res.status(500).json(err));
            });
        });
};


