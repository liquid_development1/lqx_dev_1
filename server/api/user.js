'use strict';

const bcrypt = require('bcrypt-nodejs');
const authy = require('../libs/authy');

module.exports = function(app) {
    const models = app.get('sqldb');

    app.route('/api/users')
        .post(function(req, res, next) {
            authy.registerUser({
                email: req.body.email,
                countryCode: req.body.countryCode,
                phone: req.body.phone,
            })
            .then(function(authyUser) {
                var hashedPwd = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);

                return models.users
                    .findOrCreate({
                        where: {
                            email: req.body.email,
                        },
                        defaults: {
                            name: req.body.name,
                            email: req.body.email,
                            encryptedPassword: hashedPwd,
                            countryCode: req.body.countryCode,
                            phone: req.body.phone,
                            authyId: authyUser.id,
                            verified: false,
                        },
                    });
            })
            .spread(function(user, created) {
                if (!created) {
                    return res.sendStatus(500);
                }

                user.encryptedPassword = null;

                res.json(user);
            })
            .catch(function() {
                res.sendStatus(500);
            });
        });
};
