'use strict';

const md5 = require('md5');
const mongoose = require('mongoose');
const passport = require('passport');
const querystring = require('querystring');
const request = require('request');
const uuid = require('node-uuid');

const authy = require('../libs/authy');

const Session = mongoose.model('Session');

module.exports = function(app) {
    const config = app.get('config');
    const models = app.get('sqldb');

    app.route('/api/sessions/:sid')
        .get(function(req, res, next) {
            var sid = req.params.sid;

            Session
                .findOneAndUpdate({
                    sid: sid,
                }, {
                    touchedAt: Date.now(),
                }, {
                    new: true,
                })
                .then(function(session) {
                    if (!session) {
                        throw new Error();
                    }

                    if (!session.verified) {
                        res.status(401);
                    }

                    res.json(session);
                })
                .catch(function(e) {
                    return res.status(401).json(e);
                });
        })
        .delete(function(req, res, next) {
            var sid = req.params.sid;

            req.logout();
            res.sendStatus(204);

            Session.findOneAndRemove({
                sid: sid,
            });
        })
        .put(function(req, res, next) {
            var sid = req.params.sid;

            Session
                .findOne({
                    sid: sid,
                    verified: false
                })
                .then(function(session) {
                    return Promise.all([
                        session,
                        authy.verifyToken({
                            id: session.user.authyId,
                            token: req.body.token,
                        }),
                    ]);
                })
                .spread(function(session, verifyResult) {
                    if (!verifyResult.success) {
                        throw new Error('Authy verification failed');
                    }

                    return Promise.all([
                        models.users
                            .update({
                                verified: 1,
                            }, {
                                where: {
                                    id: session.userId,
                                },
                            }),
                        Session
                            .findOneAndUpdate({
                                sid: sid,
                            }, {
                                touchedAt: Date.now(),
                                verified: true,
                            }, {
                                new: true,
                            }),
                    ]);
                })
                .then(function() {
                    res.sendStatus(204);
                })
                .catch(function(err) {
                    return res.status(401).json(err);
                });
        });

    app.route('/api/sessions/:sid/token')
        .post(function(req, res, next) {
            Session
                .findOne({
                    sid: req.params.sid,
                    verified: false,
                })
                .then((session) => {
                    return authy.requestSms({
                        id: session.user.authyId,
                    });
                })
                .then(() => res.status(204).end());
        });

    app.route('/api/sessions')
        .post(passport.authenticate('local', {
            session: false,
        }),
        function(req, res, next) {
            var sid = uuid.v4();

            var session = new Session({
                sid: sid,
                userId: req.user.id,
                user: req.user.toJSON(),
                hash: md5(req.user.email.trim().toLowerCase()),
                touchedAt: Date.now(),
                verified: !!req.user.verified,
            });

            session
                .save()
                .then(function() {
                    res.json(session);
                });
        });
};
