'use strict';

const LinkedAccount = require('mongoose').model('LinkedAccount');

module.exports = function(app) {
    app.route('/api/linked-accounts')
        .get((req, res, next) => {
            LinkedAccount
                .find()
                .exec()
                .then((linkedAccounts) => {
                    res.json(linkedAccounts);
                })
                .catch((err) => res.status(500).json(err));
        });
}
