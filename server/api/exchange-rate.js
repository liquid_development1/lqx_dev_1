'use strict';

module.exports = function(app) {
    const models = app.get('sqldb');

    app.get('/api/exchange-rates/insight', function(req, res, next) {
        var history = Number(req.query.history) || 90; // get 3 months history
        var currencies = req.query.currencies ?
            req.query.currencies.split(' ') : [];

        // get at least 2 records for calculating change, netChange
        if (history < 2) {
            history = 2;
        }

        var insight = {
            fields: currencies,
            days: history,
            currencies: {},
        };

        models.exchangeRateMas.findAll({
            order: [
                ['endOfDay', 'DESC']
            ],
            limit: history,
        }).then(function(records) {
            insight.time = records[0].endOfDay;

            var latest = records[0];
            var prev = records[1];

            // TODO should cache this logic result for avoiding block event-loop
            currencies.forEach(function(curr) {
                var data = insight.currencies[curr] = {};

                data.value = latest[curr];
                data.change = latest[curr] - prev[curr];
                data.netChange = data.change / data.value;

                data.history = [];

                records.forEach(function(record) {
                    data.history.unshift({
                        time: record.endOfDay,
                        value: record[curr],
                    });
                });
            });

            res.json(insight);
        });
    });
};
