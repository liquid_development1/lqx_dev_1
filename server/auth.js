'use strict';

//Setup local strategy
module.exports = function (app, passport) {
    passport.use(require('./passport/local'));
};
