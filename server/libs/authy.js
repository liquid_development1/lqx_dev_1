'use strict';

const querystring = require('querystring');
const request = require('request');
const config = require('../../shared/config');

exports.registerUser = function(user) {
    return createRequest({
        method: 'POST',
        url: 'https://api.authy.com/protected/json/users/new',
        qs: {
            api_key: config.twoFactorsAuth.authyApiKey,
            'user[email]': user.email,
            'user[cellphone]': user.phone,
            'user[country_code]': user.countryCode,
        },
        json: true,
    })
    .spread(function(response, body) {
        if (!body || !body.success) {
            return Promise.reject();
        }

        return body.user;
    });
};

exports.requestSms = function(user) {
    return createRequest({
        method: 'GET',
        url: 'https://api.authy.com/protected/json/sms/' + querystring.escape(user.id),
        qs: {
            api_key: config.twoFactorsAuth.authyApiKey,
            force: true,
        },
        json: true,
    })
    .spread(function(response, body) {
        return body;
    });
};

exports.verifyToken = function(user) {
    return createRequest({
        method: 'GET',
        url: 'https://api.authy.com/protected/json/verify/'
            + querystring.escape(user.token) + '/'
            + querystring.escape(user.id),
        qs: {
            api_key: config.twoFactorsAuth.authyApiKey,
        },
        json: true
    })
    .spread(function(response, body) {
        return body;
    });
};

function createRequest(params) {
    return new Promise(function(resolve, reject) {
        request(params, function(err, response, body) {
            if (err) {
                return reject(err);
            }

            resolve([response, body]);
        });
    });
}
