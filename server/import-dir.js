'use strict';

var glob = require('glob');

module.exports = function(pattern, app) {
    glob.sync(pattern).forEach(function(file) {
        require(file)(app);
    });
};
