'use strict';


const bcrypt = require('bcrypt-nodejs');
const querystring = require('querystring');

const LocalStrategy = require('passport-local').Strategy;

const models = require('../../shared/sqldb');
const config = require('../../shared/config');

const authy = require('../libs/authy');

module.exports = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
}, authenticate);

function authenticate(email, password, done) {
    models.users
        .findOne({
            where: {
                email: email,
            },
        })
        .then(function(user) {
            if (!user) {
                return done(null, false);
            }

            if (!bcrypt.compareSync(password, user.encryptedPassword)) {
                return done(null, false);
            }

            if (!config.twoFactorsAuth.enabled) {
                return done(null, user);
            }

            if (user.authyId) {
                return requestTwoFactorsAuth(user, done);
            }

            // create authy user
            authy.registerUser({
                email: user.email,
                countryCode: user.countryCode || '84',
                phone: user.phone,
            })
            .then(function(authyUser) {
                // update database to store authyId
                user.authyId = authyUser.id;

                return models.users
                    .update({
                        authyId: authyUser.id,
                    }, {
                        where: {
                            id: user.id,
                        },
                    });
            })
            .then(function() {
                requestTwoFactorsAuth(user, done);
            });
        })
        .catch(done);
}

function requestTwoFactorsAuth(user, done) {
    // send sms
    authy.requestSms({
        id: user.authyId,
    })
    .then(function(result) {
        done(null, user);
    })
    .catch(done);
}
