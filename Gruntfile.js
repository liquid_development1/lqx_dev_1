'use strict';

const _ = require('lodash');
const glob = require('glob');
const path = require('path');

module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt);

    var allConfig = {
        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            all: [
                'client',
                'server',
                'scheduler',
                'workers',
            ],
            web: [
                'client',
                'server',
            ],
        },
        watch: {
            options: {
                spawn: false,
            },
        },
    };

    glob.sync(path.resolve(__dirname, 'grunt/**/*.js')).forEach((f) => {
        var config = require(f).config;
        var register = require(f).register;

        allConfig = _.defaultsDeep(config, allConfig);

        register(grunt);
    });

    grunt.initConfig(allConfig);

    grunt.registerTask('default', [
        'concurrent:all',
    ]);

    grunt.registerTask('web', [
        'concurrent:web'
    ]);
};
