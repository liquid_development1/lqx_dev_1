'use strict';

// use .spread
global.Promise = require('bluebird');

var app = require('./server/app');
var port = app.get('config').port;

app.listen(port, function() {
    console.info('App Server started on %d', port);
});
