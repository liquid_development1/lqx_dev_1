# HOW TO RUN IN DEVELOMENT MODE
```
$grunt
```


# SOURCE CODE STRUCTURE

```
/
├─ assets/
│    ├─ css/
│    ├─ img/
│    └─ fonts/
├─ public/
│    ├─ css/ (contains minify codes)
│    ├─ img/ (contains optimized images)
│    ├─ js/ (contains minify codes)
│    ├─ fonts/
│    ├─ views/ (this should be omited, since we can inject html into js code)
│    └─ index.html
├─ client/
│    ├─ controllers/
│    ├─ factories/
│    ├─ views/
│    └─ (other files here)
├─ server/
│    └─ (other files here)
├─ Gruntfile.js
└─ package.json
```
